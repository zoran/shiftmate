//
//  LocationSearchViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

/**
 This VC enables a user to search an address via MapKit, and resolve a geolocation through CoreLocation
 */
final class LocationSearchViewController: UIViewController {
  internal static let storyboardId = "\(LocationSearchViewController.self)"

  @IBOutlet weak var suggestionsLabel: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  
  private var navSearchBar: UISearchBar?
  
  // Closure to pass back resolved location to VC instantiating this VC
  internal var completionCallback: ((_ coordinates: LocationNameCoordsResult) -> ())?
  
  private let locationSearchCompleter = MKLocalSearchCompleter()
  
  private var collectionDataStore: LocationSearchCollectionDataStore?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureNavigationBar()
    self.locationSearchCompleter.delegate = self
    self.configureCollectionView()
  }
  
  private func configureNavigationBar() {
    let searchBar = UISearchBar(frame: self.view.bounds)
    searchBar.searchBarStyle = .minimal
    searchBar.keyboardAppearance = .dark
    searchBar.placeholder = "Enter an address"
    searchBar.delegate = self
    self.navigationItem.titleView = searchBar
    self.navSearchBar = searchBar
    
    // Cancel bar button
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel",
                                                             style: .plain,
                                                             target: self,
                                                             action: #selector(self.cancelButtonTapped(_:)))
    
    self.navSearchBar?.becomeFirstResponder()
  }
  
  private func configureCollectionView() {
    // Set transparent background
    self.collectionView.backgroundColor = .clear
    self.collectionView.backgroundView = UIView(frame: .zero)
    self.collectionView.keyboardDismissMode = .onDrag
    
    // Register cells
    self.collectionView.register(UINib(nibName: LocationSearchResultCell.reuseId, bundle: nil),
                                 forCellWithReuseIdentifier: LocationSearchResultCell.reuseId)
    
    // Register data store
    self.collectionDataStore = LocationSearchCollectionDataStore(self)
    self.collectionView.dataSource = collectionDataStore
    self.collectionView.delegate = collectionDataStore
    
    // Set Collection View layout settings
    let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    flowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0) // Add padding to top/bottom of list
    flowLayout.minimumLineSpacing = 0.0 // Item Spacing
  }

  @objc func cancelButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
  
  func triggerListTap(item: String) {
    self.resolveAddressStringToCoords(addressString: item)
  }
  
  /**
   Attempt to resolve Geolocation coordinates based on a location address string
   */
  func resolveAddressStringToCoords(addressString: String) {
    CLGeocoder().geocodeAddressString(addressString) { placemarks, error in
      let placemarkLocation = placemarks?.first?.location

      guard let latitude = placemarkLocation?.coordinate.latitude,
        let longitude = placemarkLocation?.coordinate.longitude else {
          let dialog = ViewUtil.createBasicPopupDialog(title: "Location Error",
                                                       message: "Unable to determine location coordinates for selected address",
                                                       handler: nil)
          
          self.present(dialog, animated: true, completion: nil)
          return
      }
      
      // Pass coords results back to previous VC
      self.completionCallback?(LocationNameCoordsResult(address: addressString, latitude: latitude, longitude: longitude))
      self.dismiss(animated: true, completion: nil)
    }
  }
}

// MARK: - UISearchBarDelegate
extension LocationSearchViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    self.locationSearchCompleter.queryFragment = searchText // Query Location results based on SearchBar updates
  }
}

// MARK: - MKLocalSearchCompleterDelegate
extension LocationSearchViewController: MKLocalSearchCompleterDelegate {
  func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
    // Convert results list into combined string array
    let locationTitleList: [String] = completer.results.compactMap { "\($0.title), \($0.subtitle)" }

    // Update collection
    self.collectionDataStore?.setItems(itemList: locationTitleList)
  }
}
