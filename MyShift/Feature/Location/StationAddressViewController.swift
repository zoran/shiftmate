//
//  StationAddressViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC allows the user to open a location search screen, and displays the selected Location result
 */
final class StationAddressViewController: UIViewController {
  internal static let storyboardId = "\(StationAddressViewController.self)"

  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var addButton: UIButton!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var editButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .shiftTimesLocation)
  }
  
  private func configureUi() {
    self.addButton.standardButtonStyle()
    self.nextButton.ctaStyle()
    
    self.nextButton.isHidden = true
    self.addressLabel.isHidden = true
    self.editButton.isHidden = true
    
    // Attach label tap gesture
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(addressLabelSelected(_:)))
    self.addressLabel.addGestureRecognizer(tapGesture)
    self.addressLabel.isUserInteractionEnabled = true
  }
  
  /**
   Open the Location Search vc and return a resolved Location object
   */
  private func openLocationSearchVc() {
    let vc = ViewUtil.locationSearchNavigationController { location in
      self.addButton.isHidden = true
      self.descriptionLabel.isHidden = true
      
      self.nextButton.isHidden = false
      self.editButton.isHidden = false
      
      self.addressLabel.isHidden = false
      self.addressLabel.text = location.address
      

      // ZORAN: This would be a great place to do something with the resolved location object
    }
    
    self.present(vc, animated: true, completion: nil)
  }
  
  @objc func addressLabelSelected(_ selector: UITapGestureRecognizer) {
    self.openLocationSearchVc()
  }

  // MARK: - Button Delegates

  @IBAction func addButtonTapped(_ sender: Any) {
    self.openLocationSearchVc()
  }
  
  @IBAction func nextButtonTapped(_ sender: Any) {
    self.navigationController?.pushViewController(ViewUtil.commuteTimeViewController, animated: true)
  }

  @IBAction func editButtonTapped(_ sender: Any) {
    self.openLocationSearchVc()
  }
}
