//
//  LocationSearchNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 23/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This Navigation Controller displays a Location Search child VC, with Light status bar styling
 */
final class LocationSearchNavigationController: UINavigationController {
  internal static let storyboardId = "\(LocationSearchNavigationController.self)"
  
  // Closure to pass back resolved location to VC instantiating this VC
  internal var completionCallback: ((_ coordinates: LocationNameCoordsResult) -> ())?

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setNeedsStatusBarAppearanceUpdate()
    
    // Dismiss this navigation controller if completion callback was successful
    let locationSearchVc = ViewUtil.locationSearchViewController { result in
      self.completionCallback?(result)
      self.dismiss(animated: true, completion: nil)
    }

    setViewControllers([locationSearchVc], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
}
