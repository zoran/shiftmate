//
//  LocationSearchCollectionDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 18/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of item's, handles cell styling and list modification/updates
 */
final class LocationSearchCollectionDataStore: NSObject {
  fileprivate var items = [String]()
  private var parentController: LocationSearchViewController?
  
  init(_ controller: LocationSearchViewController) {
    super.init()
    self.parentController = controller
  }
  
  func setItems(itemList: [String]) {
    self.items.removeAll()
    
    self.items.append(contentsOf: itemList)
    self.parentController?.collectionView.reloadData()
  }
}

// MARK: - UICollectionViewDataSource
extension LocationSearchCollectionDataStore: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LocationSearchResultCell.reuseId, for: indexPath) as! LocationSearchResultCell
    
    cell.configureCell(dataStore: self)
    cell.populateUi(item: items[indexPath.item])
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.items.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

// MARK: - UICollectionViewDelegate
extension LocationSearchCollectionDataStore: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let selectedItem = items[indexPath.item]

    self.parentController?.triggerListTap(item: selectedItem)
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension LocationSearchCollectionDataStore: UICollectionViewDelegateFlowLayout {
  /**
   Set size of item layout
   */
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.frame.width, height: 50)
  }
}
