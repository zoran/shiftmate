//
//  LocationSearchResultCell.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom collection cell to denote a Location Search cell
 */
final class LocationSearchResultCell: UICollectionViewCell {
  internal static let reuseId = "\(LocationSearchResultCell.self)"

  @IBOutlet weak var resultLabel: UILabel!
  
  private var dataStore: LocationSearchCollectionDataStore?
  private var locationNameData: String?
  
  /**
   Configure cell values & UI
   */
  internal func configureCell(dataStore: LocationSearchCollectionDataStore) {
    self.dataStore = dataStore
  }

  /**
   Populate cell values
   */
  internal func populateUi(item: String) {
    self.resetUi()
    
    self.locationNameData = item
    
    self.resultLabel.text = item
  }
  
  private func resetUi() {
    self.locationNameData = nil
    self.resultLabel.text = ""
  }
}
