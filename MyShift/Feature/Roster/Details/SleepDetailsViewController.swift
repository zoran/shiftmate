//
//  SleepDetailsViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a recommended message on when to sleep. The user has the option to update their sleep time,
 only if a single roster activity start time is defined, and it is between 3am and 1pm
 */
final class SleepDetailsViewController: UIViewController {
  internal static let storyboardId = "\(SleepDetailsViewController.self)"

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var detailsLabel: UILabel!
  @IBOutlet weak var moreInfoButton: UIButton!
  @IBOutlet weak var editButton: UIButton!
  
  // Custom picker
  private var singleTimePickerView: SingleTimePickerView?
  private var isCustomPickerDisplayed = false
  
  var rosterActivity: RosterActivity?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.createPickerView()
    self.configureUi()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    self.singleTimePickerView?.hidePickerLayout(viewController: self)
  }
  
  private func configureUi() {
    self.editButton.isHidden = true // Hide Edit Button by default
    
    guard let rosterActivity = self.rosterActivity else { return }
    
    // Display Activity Date
    let dateString = "\(rosterActivity.startDate.dayNameFull()) \(rosterActivity.startDate.dayDateSuffix()) \(rosterActivity.startDate.monthYear())"
    self.dateLabel.text = dateString
    
    // Display Activity Time
    if let endDate = rosterActivity.endDate {
      self.timeLabel.text = "\(rosterActivity.startDate.timeTwentyFourHr()) - \(endDate.timeTwentyFourHr())"
    } else {
      self.timeLabel.text = rosterActivity.startDate.timeTwentyFourHr()
    }
    
    // Display Activity description
    let detailsString: String
    let dateIsRanged = rosterActivity.endDate != nil
    
    if dateIsRanged {
      // ZORAN: Replace "8 hours" with hour range total
      detailsString = "Making time for 8 hours of sleep is important for adapting to shift work."
    } else {
      detailsString = "Sleep as long as possible. If you cannot sleep a full 8 hour block, try to have a nap about 7-8 hours after you wake up."
    }
    
    self.detailsLabel.text = detailsString
    
    
    // Check if Activity is not a date range
    if rosterActivity.endDate == nil {
      let calendar = Calendar.current
      let startDateComponents = calendar.dateComponents([.hour, .minute], from: rosterActivity.startDate)
      
      // If start time is between 3am and 1pm, attach edit state
      if let startHour = startDateComponents.hour {
        switch startHour {
        case 3...13:
          self.attachEditState()
        default:
          return
        }
      }
    }
  }
  
  /**
   Attach tap gesture to edit shift times, underline time label, and display Edit Icon
   */
  private func attachEditState() {
    self.editButton.isHidden = false
    
    let editSleepTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.editSleepTime(_:)))
    
    self.timeLabel.isUserInteractionEnabled = true
    self.timeLabel.addGestureRecognizer(editSleepTapGesture)
  }
  
  /**
   Init SingleTimePickerView, apply constraints and callbacks
   */
  private func createPickerView() {
    let pickerView = SingleTimePickerView.instantiateNib()
    self.view.addSubview(pickerView)

    pickerView.anchorToBottomOf(viewController: self)

    // Init callbacks
    pickerView.doneCallback = { selectedTime in
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()

      self.timeLabel.text = selectedTime
    }
    
    pickerView.cancelCallback = {
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
    }
    
    pickerView.resetPickerValuesToDefaultState()
    
    self.singleTimePickerView = pickerView
  }

  /**
   Display Time Picker
   */
  @objc func editSleepTime(_ selector: UITapGestureRecognizer?) {
    self.singleTimePickerView?.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                                           completionCallback: { self.isCustomPickerDisplayed = $0 })
  }
  
  // MARK: - Button Delegates

  @IBAction func editButtonTapped(_ sender: Any) {
    self.editSleepTime(nil)
  }

  @IBAction func moreInfoButtonTapped(_ sender: Any) {
    self.present(ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.sleepInfoViewController), animated: true, completion: nil)
  }
}
