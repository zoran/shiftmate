//
//  CaffeineDetailsViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a recommended message when to consume caffeine, with date and time
 */
final class CaffeineDetailsViewController: UIViewController {
  internal static let storyboardId = "\(CaffeineDetailsViewController.self)"

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var detailsLabel: UILabel!
  @IBOutlet weak var moreInfoButton: UIButton!

  var rosterActivity: RosterActivity?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
  }
  
  private func configureUi() {
    guard let rosterActivity = self.rosterActivity else { return }
    
    // Display Activity Date
    let dateString = "\(rosterActivity.startDate.dayNameFull()) \(rosterActivity.startDate.dayDateSuffix()) \(rosterActivity.startDate.monthYear())"
    self.dateLabel.text = dateString
    
    // Display Activity Time
    if let endDate = rosterActivity.endDate {
      self.timeLabel.text = "\(rosterActivity.startDate.timeTwentyFourHr()) - \(endDate.timeTwentyFourHr())"
    } else {
      self.timeLabel.text = rosterActivity.startDate.timeTwentyFourHr()
    }
    
    // Display Activity description
    if let caffeineDrinkTitle = rosterActivity.caffeineDrinkType {
      let dateIsRanged = rosterActivity.endDate != nil
      
      let detailsString: String
      
      if dateIsRanged {
        detailsString = "To ensure optimal sleep, you should finish your last \(caffeineDrinkTitle) at no later than \(rosterActivity.endDate!.timeTwentyFourHr())."
      } else {
        detailsString = "To maximise alertness, \(rosterActivity.startDate.timeTwentyFourHr()) is a perfect time to have a \(caffeineDrinkTitle)."
      }
      
      self.detailsLabel.text = detailsString
    }
  }
  
  // MARK: - Button Delegates

  @IBAction func moreInfoButtonTapped(_ sender: Any) {
    self.present(ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.caffeineInfoViewController), animated: true, completion: nil)
  }
}
