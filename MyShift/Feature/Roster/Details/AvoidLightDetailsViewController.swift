//
//  AvoidLightDetailsViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a recommended message when to avoid light, with date and time
 */
final class AvoidLightDetailsViewController: UIViewController {
  internal static let storyboardId = "\(AvoidLightDetailsViewController.self)"

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var detailsLabel: UILabel!
  @IBOutlet weak var moreInfoButton: UIButton!

  var rosterActivity: RosterActivity?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
  }
  
  private func configureUi() {
    guard let rosterActivity = self.rosterActivity else { return }
    
    // Display Activity Date
    let dateString = "\(rosterActivity.startDate.dayNameFull()) \(rosterActivity.startDate.dayDateSuffix()) \(rosterActivity.startDate.monthYear())"
    self.dateLabel.text = dateString
    
    // Display Activity Time
    if let endDate = rosterActivity.endDate {
      self.timeLabel.text = "\(rosterActivity.startDate.timeTwentyFourHr()) - \(endDate.timeTwentyFourHr())"
    } else {
      self.timeLabel.text = rosterActivity.startDate.timeTwentyFourHr()
    }
    
    // Display Activity description
    let dateIsRanged = rosterActivity.endDate != nil
    
    let detailsString: String
    
    if dateIsRanged {
      detailsString = "To maximise your alertness at work, aim for at least 15 minutes of bright blue or white light exposure from the beginning of your shift to an hour before your bedtime."
    } else {
      detailsString = "For optimal sleep, avoid bright blue and white light from \(rosterActivity.startDate.timeTwentyFourHr()). Put your phone into Night Shift Mode or try to avoid all electronic devices completely."
    }
    
    self.detailsLabel.text = detailsString
  }

  // MARK: - Button Delegates

  @IBAction func moreInfoButtonTapped(_ sender: Any) {
    self.present(ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.lightInfoViewController), animated: true, completion: nil)
  }
}
