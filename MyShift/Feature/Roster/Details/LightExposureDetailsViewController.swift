//
//  LightExposureDetailsViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a recommended message when to be exposed to light, with date and time
 */
final class LightExposureDetailsViewController: UIViewController {
  internal static let storyboardId = "\(LightExposureDetailsViewController.self)"

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var moreInfoButton: UIButton!

  var rosterActivity: RosterActivity?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
  }
  
  private func configureUi() {
    guard let rosterActivity = self.rosterActivity else { return }
    
    // Display Activity Date
    let dateString = "\(rosterActivity.startDate.dayNameFull()) \(rosterActivity.startDate.dayDateSuffix()) \(rosterActivity.startDate.monthYear())"
    self.dateLabel.text = dateString
    
    // Display Activity Time
    if let endDate = rosterActivity.endDate {
      self.timeLabel.text = "\(rosterActivity.startDate.timeTwentyFourHr()) - \(endDate.timeTwentyFourHr())"
    } else {
      self.timeLabel.text = rosterActivity.startDate.timeTwentyFourHr()
    }
  }
  
  // MARK: - Button Delegates

  @IBAction func moreInfoButtonTapped(_ sender: Any) {
    self.present(ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.lightInfoViewController), animated: true, completion: nil)
  }
}
