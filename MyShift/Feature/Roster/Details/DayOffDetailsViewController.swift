//
//  DayOffDetailViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a simple "Day Off" message with option to add a shift
 */
final class DayOffDetailsViewController: UIViewController {
  internal static let storyboardId = "\(DayOffDetailsViewController.self)"

  @IBOutlet weak var dateLabel: UILabel!  
  @IBOutlet weak var addShiftButton: UIButton!
  
  var rosterActivity: RosterActivity?

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
  }
  
  private func configureUi() {
    self.addShiftButton.standardButtonStyle()
    
    guard let rosterActivity = self.rosterActivity else { return }
    
    // Display Activity Date
    let dateString = "\(rosterActivity.startDate.dayNameFull()) \(rosterActivity.startDate.dayDateSuffix()) \(rosterActivity.startDate.monthYear())"
    self.dateLabel.text = dateString
  }

  // MARK: - Button Delegates

  @IBAction func addShiftButtonTapped(_ sender: Any) {
    
    let addSingleDayRosterVc = ViewUtil.createSingleShiftNavigationController(
      successCallback: {
        // Pop current VC to return to previous VC
        self.navigationController?.popViewController(animated: true)
        
        // ZORAN: You might want to refresh the list on the screen displayed when the above pop function is triggered
      },
      cancelCallback: {
        // Add Single Shift cancelled
      },
      contextDate: rosterActivity?.startDate
    )
    
    self.present(addSingleDayRosterVc, animated: true, completion: nil)
  }
}
