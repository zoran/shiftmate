//
//  WorkDetailsViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a "Work" type of roster activity, with information regarding the users shift. The user can update the
 time, add a note, and adjust the overtime hours
 */
final class WorkDetailsViewController: UIViewController {
  internal static let storyboardId = "\(WorkDetailsViewController.self)"

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var shiftTypeLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var overtimeValueLabel: UILabel!
  @IBOutlet weak var addOvertimeButton: UIButton!
  @IBOutlet weak var minusOvertimeButton: UIButton!
  @IBOutlet weak var saveShiftButton: UIButton!
  @IBOutlet weak var addNoteBarButton: UIBarButtonItem!
  @IBOutlet weak var editShiftIcon: UIImageView!
  @IBOutlet weak var shiftNotePreviewLabel: UILabel!
  @IBOutlet weak var shiftNoteView: UIView!
  
  var rosterActivity: RosterActivity?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
    self.populateWorkDetails()
  }
  
  private func configureUi() {
    self.saveShiftButton.ctaStyle()

    self.shiftNoteView.isHidden = true

    self.overtimeValueLabel.layer.borderWidth = 1
    self.overtimeValueLabel.layer.borderColor = ViewUtil.overtimeBorderColor.cgColor
    
    self.addOvertimeButton.setTitleColor(ViewUtil.standardButtonBorderColor, for: .normal)
    self.addOvertimeButton.layer.borderWidth = 1
    self.addOvertimeButton.layer.borderColor = ViewUtil.overtimeBorderColor.cgColor
    self.addOvertimeButton.roundedRightBorder(radius: 22)
    
    self.minusOvertimeButton.setTitleColor(ViewUtil.standardButtonBorderColor, for: .normal)
    self.minusOvertimeButton.layer.borderWidth = 1
    self.minusOvertimeButton.layer.borderColor = ViewUtil.overtimeBorderColor.cgColor
    self.minusOvertimeButton.roundedLeftBorder(radius: 22)
    
    self.updateOvertimeLabel() // Display overtime hours
    
    // Attach tap gesture to edit shift times
    let editShiftTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.displayEditShift(_:)))
    self.editShiftIcon.isUserInteractionEnabled = true
    self.editShiftIcon.addGestureRecognizer(editShiftTapGesture)
    self.timeLabel.isUserInteractionEnabled = true
    self.timeLabel.addGestureRecognizer(editShiftTapGesture)
  }
  
  private func populateWorkDetails() {
    guard let rosterActivity = self.rosterActivity else { return }
    
    // Display Activity Date
    let dateString = "\(rosterActivity.startDate.dayNameFull()) \(rosterActivity.startDate.dayDateSuffix()) \(rosterActivity.startDate.monthYear())"
    self.dateLabel.text = dateString
    
    // Display Shift Type
    if let shiftType = rosterActivity.workShiftType {
      self.shiftTypeLabel.text = shiftType
    }
    
    // Display Activity Time
    if let endDate = rosterActivity.endDate {
      self.timeLabel.text = "\(rosterActivity.startDate.timeTwentyFourHr()) - \(endDate.timeTwentyFourHr())"
    } else {
      self.timeLabel.text = rosterActivity.startDate.timeTwentyFourHr()
    }
    
    // Display shift note in preview label
    if let workNote = rosterActivity.workNotes {
      self.shiftNotePreviewLabel.text = workNote
      
      self.shiftNoteView.isHidden = workNote.count == 0
    }
  }
  
  /**
   Update the overtime label based on defined overtime hours for the roster activity
   */
  private func updateOvertimeLabel() {
    if let overtimeHours = self.rosterActivity?.workOvertimeHours {
      if overtimeHours <= 1 {
        self.overtimeValueLabel.text = "\(overtimeHours) hr"
      } else if overtimeHours >= 2 {
        self.overtimeValueLabel.text = "\(overtimeHours) hrs"
      }
    }
  }
  
  /**
   Display an alert controller to add or modify the shift note for this roster activity
   */
  private func showAddNoteDialog() {
    let alertController = UIAlertController(title: "Add Note", message: "", preferredStyle: .alert)
    
    alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: { alert -> Void in
      let textField = alertController.textFields![0] as UITextField
      
      // Save input text to roster activity work notes
      if let inputText = textField.text {
        self.rosterActivity?.workNotes = inputText
        self.shiftNotePreviewLabel.text = inputText // Display updated note in preview label
        
        self.shiftNoteView.isHidden = inputText.count == 0
      }
    }))
    
    alertController.addTextField(configurationHandler: { textField in
      textField.placeholder = "Enter a note for your rostered shift"
      
      // Prepopulate with existing note if present
      if let workNotes = self.rosterActivity?.workNotes {
        textField.text = workNotes
      }
    })
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  /**
   Open Add Single Shift navigation controller to update values of current roster activity
   */
  @objc func displayEditShift(_ selector: UITapGestureRecognizer) {
    let addSingleDayRosterVc = ViewUtil.createSingleShiftNavigationController(
      successCallback: {
        // ZORAN: This would be a good spot to remove this existing shift, since a new shift would have been created
        // in place of this one (if that was not already taken care of when creating a new shift with different time & type)

        // Pop current VC to return to previous VC
        self.navigationController?.popViewController(animated: true)
      },
      cancelCallback: {
        // Update Shift cancelled
      },
      contextDate: self.rosterActivity?.startDate
    )
    
    self.present(addSingleDayRosterVc, animated: true, completion: nil)
  }

  
  // MARK: - Button delegates

  /**
   Increment the total work overtime hours for the roster activity
   */
  @IBAction func addOvertimeButtonTapped(_ sender: Any) {
    guard let rosterActivity = self.rosterActivity else {
      return
    }

    if let overtimeHours = rosterActivity.workOvertimeHours {
      self.rosterActivity?.workOvertimeHours = overtimeHours + 1
    } else {
      self.rosterActivity?.workOvertimeHours = 0
    }

    self.updateOvertimeLabel()
  }

  /**
   Decrement the total work overtime hours for the roster activity
   */
  @IBAction func minusOvertimeButtonTapped(_ sender: Any) {
    if let overtimeHours = rosterActivity?.workOvertimeHours {
      if overtimeHours <= 0 {
        return
      }
      
      self.rosterActivity?.workOvertimeHours = overtimeHours - 1
    } else {
      self.rosterActivity?.workOvertimeHours = 0
    }
    
    self.updateOvertimeLabel()
  }

  @IBAction func addNoteBarButtonTapped(_ sender: Any) {
    self.showAddNoteDialog()
  }

  @IBAction func updateShiftButtonTapped(_ sender: Any) {
    // ZORAN: This would be a good spot to persist any updates to the existing shift, such as the note text
    // and overtime hours
    
    self.navigationController?.popViewController(animated: true)
  }
}
