//
//  RosterListCollectionDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of item's, handles cell styling and list modification/updates
 */
final class RosterListCollectionDataStore: NSObject {
  fileprivate var items = [RosterActivity]()
  private var parentController: RosterListViewController?
  
  init(_ controller: RosterListViewController) {
    super.init()
    self.parentController = controller
  }

  /**
   Filter out any RosterActivity objects which aren't of type "Work" or "Day Off", and update list
   */
  func setItems(itemList: [RosterActivity]) {
    self.items.removeAll()

    let filteredList = itemList.filter { return $0.activityType == .work || $0.activityType == .dayOff }
    self.items.append(contentsOf: filteredList)

    self.parentController?.collectionView.reloadData()
  }
  
  func getItems() -> [RosterActivity] {
    return self.items
  }
  
  func triggerOpenActivity(item: RosterActivity) {
    self.parentController?.triggerOpenActivity(item: item)
  }
}

// MARK: - UICollectionViewDataSource
extension RosterListCollectionDataStore: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RosterActivityCell.reuseId, for: indexPath) as! RosterActivityCell
    
    cell.configureCell(dataStore: self)
    cell.populateUi(item: items[indexPath.item])
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.items.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension RosterListCollectionDataStore: UICollectionViewDelegateFlowLayout {
  /**
   Set size of item layout
   */
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.frame.width, height: 50)
  }
}


// MARK: - UICollectionViewDelegate
extension RosterListCollectionDataStore: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RosterActivityCell.reuseId, for: indexPath) as! RosterActivityCell
    
    cell.cellFinishedDisplaying()
  }
}
