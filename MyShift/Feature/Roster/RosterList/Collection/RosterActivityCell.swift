//
//  RosterActivityCell.swift
//  MyShift
//
//  Created by Michael Tigas on 16/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom collection cell to denote a Roster Activity cell
 */
final class RosterActivityCell: UICollectionViewCell {
  internal static let reuseId = "\(RosterActivityCell.self)"

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var activityType: UILabel!
  @IBOutlet weak var activityLogo: UIImageView!
  
  private var dataStore: RosterListCollectionDataStore?
  private var rosterActivity: RosterActivity?

  /**
   Configure cell values & UI
   */
  internal func configureCell(dataStore: RosterListCollectionDataStore) {
    self.dataStore = dataStore
    
    self.configureGestures()
  }
  
  /**
   Populate cell values
   */
  internal func populateUi(item: RosterActivity) {
    self.resetUi()

    self.rosterActivity = item
    
    // Display activity start date
    self.dateLabel.text = "\(item.startDate.dayName().uppercased()) \(item.startDate.dayDate()) \(item.startDate.month().uppercased())"
    	
    // Set activity icon
    self.activityLogo.image = item.activityType.icon
    
    // Set activity time and type
    if item.activityType == .work {
      if item.endDate == nil {
        self.timeLabel.text = item.startDate.timeTwentyFourHr()
      } else {
        self.timeLabel.text = "\(item.startDate.timeTwentyFourHr()) - \(item.endDate!.timeTwentyFourHr())"
      }
      
      // Display activity shift type if present
      if let shiftType = item.workShiftType {
        self.activityType.text = shiftType
      }
      
      self.timeLabel.font = UIFont.systemFont(ofSize: 13, weight: .light)
    } else if item.activityType == .dayOff {
      self.timeLabel.text = item.activityType.name
      self.timeLabel.font = UIFont.systemFont(ofSize: 13, weight: .medium)
    }
  }
  
  private func configureGestures() {
    // General Background View Tap
    let backgroundTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundViewTapped(_:)))
    self.contentView.addGestureRecognizer(backgroundTapGesture)
  }
  
  /**
   Send open activity action via DataStore
   */
  @objc func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
    if let activity = rosterActivity { self.dataStore?.triggerOpenActivity(item: activity) }
  }

  internal func cellFinishedDisplaying() {
    // Remove tap gestures
    if let gestureRecognizers = self.contentView.gestureRecognizers {
      for gesture in gestureRecognizers {
        self.contentView.removeGestureRecognizer(gesture)
      }
    }
  }

  private func resetUi() {
    self.rosterActivity = nil
    self.dateLabel.text = ""
    self.timeLabel.text = ""
    self.activityType.text = ""
    self.activityLogo.image = nil
  }
}
