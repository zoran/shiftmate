//
//  RosterListViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit
import FSCalendar

/**
 This VC displays a list containing the users rostered work and day off schedule
 */
final class RosterListViewController: UIViewController {
  internal static let storyboardId = "\(RosterListViewController.self)"

  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var calendarContainerView: UIView!
  @IBOutlet weak var calendarCurrentMonthLabel: UILabel!
  
  private var collectionDataStore: RosterListCollectionDataStore?
  
  // Calendar
  var fsCalendar: FSCalendar?
  private var calendarDataStore: RosterCalendarDataStore?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureNavigationBar()
    self.configureCollectionView()
    self.attachCalendarToView()
  }
  
  /**
   Set VC navigation item's title view as a UILabel "Roster"
   */
  private func configureNavigationBar() {
    let titleLabel = UILabel(frame: self.view.bounds)
    titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .light)
    titleLabel.text = "ROSTER"
    titleLabel.textAlignment = .center
    titleLabel.textColor = .white

    self.navigationItem.titleView = titleLabel
  }
  
  private func attachCalendarToView() {
    // Init calendar to match container view bounds
    let calendar = FSCalendar(frame: self.calendarContainerView.bounds)
    
    self.calendarDataStore = RosterCalendarDataStore(self)
    calendar.dataSource = self.calendarDataStore
    calendar.delegate = self.calendarDataStore
    
    calendar.applyCustomStyle()
    
    // Register StyledCalendarDayViewCell for use in creating new calendar cells
    calendar.register(CalendarDateViewCell.self, forCellReuseIdentifier: CalendarDateViewCell.reuseId)
    
    // Add calendar to container
    self.calendarContainerView.addSubview(calendar)

    // Constrain calendar to its specific container view
    calendar.centerXAnchor.constraint(equalTo: self.calendarContainerView.centerXAnchor).isActive = true
    calendar.centerYAnchor.constraint(equalTo: self.calendarContainerView.centerYAnchor).isActive = true
    calendar.leadingAnchor.constraint(equalTo: self.calendarContainerView.leadingAnchor).isActive = true
    calendar.trailingAnchor.constraint(equalTo: self.calendarContainerView.trailingAnchor).isActive = true
    calendar.topAnchor.constraint(equalTo: self.calendarContainerView.topAnchor).isActive = true
    calendar.bottomAnchor.constraint(equalTo: self.calendarContainerView.bottomAnchor).isActive = true
    calendar.translatesAutoresizingMaskIntoConstraints = false
    
    self.fsCalendar = calendar
    
    // Set current month label
    self.updateCalendarHeaderLabel()

    // Pre-select the week from today's date
    if let todayDate = calendar.today {
      self.calendarDataStore?.selectFollowingWeekFromDate(todayDate, calendar: calendar)
    }
  }
  
  /**
   Update month displayed in calendar header
   */
  private func updateCalendarHeaderLabel() {
    guard let calendar = self.fsCalendar else { return }
    self.calendarCurrentMonthLabel.text = calendar.currentPage.monthYear().uppercased()
  }

  /**
   Move back or forward a month
   */
  func updateCalendarMonth(increment: Bool) {
    guard let calendarPage = self.fsCalendar?.currentPage else { return }
    
    if let newPage = self.fsCalendar?.gregorian.date(byAdding: .month, value: increment ? 1 : -1, to: calendarPage) {
      self.fsCalendar?.currentPage = newPage
    }
  }
  
  /**
   Callback from Calendar data store when calendar date-range has been updated
   */
  func calendarSelectionWasUpdated() {
    // ZORAN: You will need to fetch the current users activity schedule and remove the following line
    var dateOrderedRosterList = MockDataUtil.activityListExample() // Replace with live data source

    dateOrderedRosterList.sort { $0.startDate < $1.startDate } // Order shifts by starting date
    
    // Filter shifts by date range
    var dateFilteredList: [RosterActivity] = []
    guard let calendar = self.fsCalendar,
      let calendarStartDate = calendar.selectedDates.orderAscending().first,
      let calendarEndDate = calendar.selectedDates.orderAscending().last,
      let adjustedEndDate = calendar.gregorian.date(bySettingHour: 23, minute: 59, second: 59, of: calendarEndDate) else {
        // Set empty list
        self.collectionDataStore?.setItems(itemList: [])
        return
    }

    // Add shift to filtered list if start date is within selected date boundaries
    for shift in dateOrderedRosterList {
      if shift.startDate >= calendarStartDate && shift.startDate <= adjustedEndDate {
        dateFilteredList.append(shift)
      }
    }
    
    self.collectionDataStore?.setItems(itemList: dateFilteredList)
  }

  private func configureCollectionView() {
    // Set transparent background
    self.collectionView.backgroundColor = .clear
    self.collectionView.backgroundView = UIView(frame: .zero)
    self.collectionView.keyboardDismissMode = .onDrag
    
    // Register cells
    self.collectionView.register(UINib(nibName: RosterActivityCell.reuseId, bundle: nil),
                                 forCellWithReuseIdentifier: RosterActivityCell.reuseId)
    
    // Register data store
    self.collectionDataStore = RosterListCollectionDataStore(self)
    self.collectionView.dataSource = collectionDataStore
    self.collectionView.delegate = collectionDataStore
    
    // Set Collection View layout settings
    let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    flowLayout.sectionInset = UIEdgeInsets(top: 16.0, left: 0.0, bottom: 0.0, right: 0.0) // Add padding to top/bottom of list
    flowLayout.minimumLineSpacing = 1.0 // Item Spacing
  }
  
  /**
   Open a ViewController based on the RosterActivity type
   */
  func triggerOpenActivity(item: RosterActivity) {
    let viewController: UIViewController
    
    switch item.activityType {
    case .work:           viewController = ViewUtil.workDetailsViewController(withActivity: item)
    case .dayOff:         viewController = ViewUtil.dayOffDetailsViewController(withActivity: item)
    default: return
    }
    
    self.navigationController?.pushViewController(viewController, animated: true)
  }
  
  /**
   Export all listed roster activities
   */
  private func exportRosterActivityList(list: [RosterActivity]) {
    var exportText = ""
    
    list.forEach { exportText = exportText + $0.exportActivityText() }
    
    self.showShareVc(text: exportText)
  }
  
  /**
   Display Share VC picker
   */
  private func showShareVc(text: String) {
    let vc = UIActivityViewController(activityItems: [text], applicationActivities: nil)
    self.present(vc, animated: true)
  }
  
  // MARK: - Button Delegates
  
  @IBAction func calendarForwardButtonTapped(_ sender: Any) {
    self.updateCalendarMonth(increment: true)
    self.updateCalendarHeaderLabel()
  }
  
  @IBAction func calendarBackButtonTapped(_ sender: Any) {
    self.updateCalendarMonth(increment: false)
    self.updateCalendarHeaderLabel()
  }

  @IBAction func addButtonTapped(_ sender: Any) {
    self.present(ViewUtil.addRosterNavigationController, animated: true, completion: nil)
  }

  @IBAction func shareButtonTapped(_ sender: Any) {
    guard let items = self.collectionDataStore?.getItems() else { return }
    
    self.exportRosterActivityList(list: items)
  }
}
