//
//  RosterCalendarDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 25/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation
import FSCalendar

/**
 This data store handles necessary Calendar cell configuration for the Roster List VC
 */
final class RosterCalendarDataStore: NSObject {
  private var parentController: RosterListViewController?
  
  init(_ controller: RosterListViewController) {
    super.init()
    self.parentController = controller
  }
  
  /**
   Reconfigure all visible date cells
   */
  private func configureAllVisibleCells(_ calendar: FSCalendar) {
    calendar.visibleCells()
      .forEach({ cell in
        if let date = calendar.date(for: cell) {
          let position = calendar.monthPosition(for: cell)
          
          self.configureDateCell(cell, for: date, at: position)
        }
      })
  }
  
  /**
   Configure the date cell's selection type
   */
  private func configureDateCell(_ cell: FSCalendarCell,
                                 for date: Date,
                                 at position: FSCalendarMonthPosition) {
    guard let dateCell = cell as? CalendarDateViewCell else { return }
    
    switch position {
    case .current:
      let selectedDates = cell.calendar.selectedDates
      var selectionType = CalendarDateViewCell.SelectionType.none
      
      // Determine selection type based on date ranges
      if !selectedDates.contains(date) {
        selectionType = .none
        dateCell.selectionLayer.isHidden = true
        return
      }
      
      if let previousDate = cell.calendar.gregorian.date(byAdding: .day, value: -1, to: date),
        let nextDate = cell.calendar.gregorian.date(byAdding: .day, value: 1, to: date) {
        
        if selectedDates.contains(date) {
          if selectedDates.contains(previousDate) && selectedDates.contains(nextDate) {
            selectionType = .middle
          } else if selectedDates.contains(previousDate) && selectedDates.contains(date) {
            selectionType = .rightBorder
          } else if selectedDates.contains(nextDate) {
            selectionType = .leftBorder
          } else {
            selectionType = .single
          }
        }
      }
      
      dateCell.selectionLayer.isHidden = false
      dateCell.selectionType = selectionType
    default:
      dateCell.selectionLayer.isHidden = true
    }
  }
  
  /**
   Deselect all current selected calendar dates, and select the following week starting from the date parameter
   */
  func selectFollowingWeekFromDate(_ date: Date, calendar: FSCalendar) {
    for currentSelectedDate in calendar.selectedDates {
      calendar.deselect(currentSelectedDate)
    }
    
    for value in 0...6 {
      let nextDate = calendar.gregorian.date(byAdding: .day, value: value, to: date)
      calendar.select(nextDate)
    }
    
    calendar.currentPage = date // Remain on current month page if selection overlaps into following month

    self.parentController?.calendarSelectionWasUpdated()
  }
}

// MARK: - FSCalendarDataSource
extension RosterCalendarDataStore: FSCalendarDataSource {
  func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
    let cell = calendar.dequeueReusableCell(withIdentifier: CalendarDateViewCell.reuseId, for: date, at: position)
    
    // If calendar cell is of type CalendarDateViewCell, apply custom formatting
    if let customCell = cell as? CalendarDateViewCell {
      customCell.applyTitleFormatting()
    }
    
    return cell
  }
  
  func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
    self.configureDateCell(cell, for: date, at: monthPosition) // Configure Date Cell when displayed
  }
  
  func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
    self.selectFollowingWeekFromDate(date, calendar: calendar)
    self.configureAllVisibleCells(calendar) // Reconfigure all date cells
  }
  
  func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
    self.selectFollowingWeekFromDate(date, calendar: calendar)
    self.configureAllVisibleCells(calendar) // Reconfigure all date cells
  }
  
  func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
    return monthPosition == .current
  }
  
  func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
    return monthPosition == .current
  }
}

// MARK: - FSCalendarDelegate
extension RosterCalendarDataStore: FSCalendarDelegate {
  func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
    self.parentController?.fsCalendar?.frame = CGRect(origin: calendar.frame.origin, size: bounds.size)
  }
}
