//
//  RosterNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This Navigation Controller handles displaying the Schedule/Roster list screens and subsequent selected details vc's
 */
final class RosterNavigationController: UINavigationController {
  internal static let storyboardId = "\(RosterNavigationController.self)"
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.setNeedsStatusBarAppearanceUpdate()
    self.delegate = self
    self.useCustomBackIndicator()
    
    self.setViewControllers([ViewUtil.scheduleListViewController], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
}

// MARK: - UINavigationControllerDelegate
extension RosterNavigationController: UINavigationControllerDelegate {
  func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    // Do not display a VC title
    // This will also prevent previous VC title being shown in Back button
    viewController.title = " "
  }
}
