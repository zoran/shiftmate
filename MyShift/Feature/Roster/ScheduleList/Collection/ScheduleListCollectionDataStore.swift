//
//  ScheduleListCollectionDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of item's, handles cell styling and list modification/updates
 */
final class ScheduleListCollectionDataStore: NSObject {
  fileprivate var items = [RosterActivity]()
  private var parentController: ScheduleListViewController?
  
  init(_ controller: ScheduleListViewController) {
    super.init()
    self.parentController = controller
  }
  
  func setItems(itemList: [RosterActivity]) {
    self.items.removeAll()
    
    self.items.append(contentsOf: itemList)
    self.parentController?.collectionView.reloadData()
  }
  
  func hasItems() -> Bool {
    return !self.items.isEmpty
  }  

  func triggerOpenActivity(item: RosterActivity) {
    self.parentController?.triggerOpenActivity(item: item)
  }
}

// MARK: - UICollectionViewDataSource
extension ScheduleListCollectionDataStore: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ScheduleActivityCell.reuseId, for: indexPath) as! ScheduleActivityCell
    
    cell.configureCell(dataStore: self)
    cell.populateUi(item: items[indexPath.item])

    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.items.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ScheduleListCollectionDataStore: UICollectionViewDelegateFlowLayout {
  /**
   Set size of item layout
   */
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    // Add additional height to cell if data model is the first scheduled roster activity for the day
    if self.items[indexPath.item].isFirstScheduledForDay {
      return CGSize(width: collectionView.frame.width, height: 70)
    } else {
      return CGSize(width: collectionView.frame.width, height: 60)
    }
  }
}

// MARK: - UICollectionViewDelegate
extension ScheduleListCollectionDataStore: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ScheduleActivityCell.reuseId, for: indexPath) as! ScheduleActivityCell
    
    cell.cellFinishedDisplaying()
  }
}
