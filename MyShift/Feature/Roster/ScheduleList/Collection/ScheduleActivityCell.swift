//
//  ScheduleActivityCell.swift
//  MyShift
//
//  Created by Michael Tigas on 16/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom collection cell to denote a Schedule Activity cell
 */
final class ScheduleActivityCell: UICollectionViewCell {
  internal static let reuseId = "\(ScheduleActivityCell.self)"

  @IBOutlet weak var dateStackView: UIStackView!
  @IBOutlet weak var activityInfoStackView: UIStackView!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var dayLabel: UILabel!
  @IBOutlet weak var dimpleIndicator: UIImageView!
  
  @IBOutlet weak var activityIcon: UIImageView!
  @IBOutlet weak var activityTypeLabel: UILabel!
  @IBOutlet weak var activityTimeLabel: UILabel!

  @IBOutlet weak var activityBackgroundView: UIView!
  @IBOutlet weak var activitySuffixColorView: UIView!

  @IBOutlet weak var overtimeView: UIView!
  @IBOutlet weak var overtimeValueLabel: UILabel!
  
  private var dataStore: ScheduleListCollectionDataStore?
  private var rosterActivity: RosterActivity?

  /**
   Configure cell values & UI
   */
  internal func configureCell(dataStore: ScheduleListCollectionDataStore) {
    self.dataStore = dataStore

    self.configureGestures()
    self.configureUi()
  }
  
  private func configureUi() {
    self.activityBackgroundView.layer.masksToBounds = true
    self.activityBackgroundView.layer.cornerRadius = 4
    
    self.overtimeView.layer.masksToBounds = true
    self.overtimeView.layer.cornerRadius = 8
    
    self.dateStackView.isUserInteractionEnabled = false
    self.activityInfoStackView.isUserInteractionEnabled = false
  }
  
  /**
   Populate cell values
   */
  internal func populateUi(item: RosterActivity) {
    self.resetUi()

    self.rosterActivity = item
    
    // Set activity type label
    self.activityTypeLabel.text = item.activityType.name

    // If Activity Type = Work, update label with shift type
    if item.activityType == .work,
      let shiftType = item.workShiftType {
        self.activityTypeLabel.text = "\(item.activityType.name): \(shiftType)"
    } else if item.activityType == .sleep {
      if item.endDate == nil {
        // If there is no sleep time range
        self.activityTypeLabel.text = "Recommended Bed Time"
      } else {
        self.activityTypeLabel.text = "Sleep"
      }
    }
  
    // Set activity icon
    if let activityIcon = item.activityType.icon { self.activityIcon.image = activityIcon }
    
    // If Activity Type = Work, display overtime hours
    if item.activityType == .work,
      let overtimeHours = item.workOvertimeHours,
      overtimeHours > 0 {
      self.overtimeView.isHidden = false
      self.overtimeValueLabel.text = "+\(overtimeHours)"
    } else {
      self.overtimeView.isHidden = true
    }
    
    // Display activity start dates for the first scheduled activity for the day
    if item.isFirstScheduledForDay {
      self.dateLabel.text = item.startDate.dayDate()
      self.dayLabel.text = item.startDate.dayName().uppercased()
    }

    // Display Start and End Time, based on whether activity type is Day Off, and presence of End Date
    if item.activityType == .dayOff {
      self.activityTimeLabel.text = "No Shifts"
    } else {
      if item.endDate == nil {
        self.activityTimeLabel.text = item.startDate.timeTwentyFourHr()
      } else {
        self.activityTimeLabel.text = "\(item.startDate.timeTwentyFourHr()) - \(item.endDate!.timeTwentyFourHr())"
      }
    }

    // Apply background color based on current Time conditions
    if item.endDate != nil {
      let isAfterStart = item.startDate < Date()
      let isBeforeFinish = Date() < item.endDate!
      let activityIsLive = isAfterStart && isBeforeFinish
      
      // Check if current activity is considered live
      if activityIsLive {
        self.activityBackgroundView.backgroundColor = item.activityType.color
        self.activitySuffixColorView.backgroundColor = .clear
      } else {
        self.activityBackgroundView.backgroundColor = ViewUtil.rosterActivityBackgroundColor
        self.activitySuffixColorView.backgroundColor = item.activityType.color
      }
      
      self.dimpleIndicator.isHidden = !activityIsLive
    } else {
      self.activityBackgroundView.backgroundColor = ViewUtil.rosterActivityBackgroundColor
      self.activitySuffixColorView.backgroundColor = item.activityType.color
    }
  }

  private func configureGestures() {
    // General Background View Tap
    let backgroundLayoutTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundViewTapped(_:)))
    self.activityBackgroundView.addGestureRecognizer(backgroundLayoutTapGesture)
  }
  
  /**
   Send open activity action via DataStore
   */
  @objc func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
    if let activity = rosterActivity {
      self.dataStore?.triggerOpenActivity(item: activity)
    }
  }
  
  internal func cellFinishedDisplaying() {
    // Remove tap gestures
    if let gestureRecognizers = self.activityBackgroundView.gestureRecognizers {
      for gesture in gestureRecognizers {
        self.activityBackgroundView.removeGestureRecognizer(gesture)
      }
    }
  }

  private func resetUi() {
    self.rosterActivity = nil
    self.activityTypeLabel.text = ""
    self.activityTimeLabel.text = ""
    self.activityIcon.image = nil
    self.overtimeValueLabel.text = ""
    self.dateLabel.text = ""
    self.dayLabel.text = ""
    
    self.dimpleIndicator.isHidden = true
    
    self.activityBackgroundView.backgroundColor = .clear
    self.activitySuffixColorView.backgroundColor = .clear
  }
}
