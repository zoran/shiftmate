//
//  ScheduleListViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a list containing the users general schedule
 */
final class ScheduleListViewController: UIViewController {
  internal static let storyboardId = "\(ScheduleListViewController.self)"

  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var autoRosterPlaceholderView: UIView!
  @IBOutlet weak var manualRosterStackView: UIStackView!
  @IBOutlet weak var addRosterButton: UIButton!

  private var collectionDataStore: ScheduleListCollectionDataStore?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.addRosterButton.ctaStyle()
    self.autoRosterPlaceholderView.isHidden = true
    self.manualRosterStackView.isHidden = true
    
    self.configureNavigationBar()
    self.configureCollectionView()
    
    // ZORAN: This would be a great spot to fetch the rostered schedule from the DB, and remove the mock data call below
    self.collectionDataStore?.setItems(itemList: MockDataUtil.activityListExample())
    
    // ZORAN: Uncomment the below line if the user has configured the Automatic roster input type
    // self.autoRosterPlaceholderView.isHidden = false
    
    // ZORAN: Uncomment the below block if the user has configured the Manual roster input type
    if let hasItems = self.collectionDataStore?.hasItems() {
      self.manualRosterStackView.isHidden = hasItems
    }
  }
  
  /**
   Set VC title as current month
   */
  private func configureNavigationBar() {
    let currentMonthTitle = UILabel(frame: self.view.bounds)
    currentMonthTitle.font = UIFont.systemFont(ofSize: 16, weight: .light)
    currentMonthTitle.text = Date().monthYear().uppercased()
    currentMonthTitle.textAlignment = .center
    currentMonthTitle.textColor = .white
    self.navigationItem.titleView = currentMonthTitle
  }

  private func configureCollectionView() {
    // Set transparent background
    self.collectionView.backgroundColor = .clear
    self.collectionView.backgroundView = UIView(frame: .zero)
    self.collectionView.keyboardDismissMode = .onDrag
    
    // Register cells
    self.collectionView.register(UINib(nibName: ScheduleActivityCell.reuseId, bundle: nil),
                                 forCellWithReuseIdentifier: ScheduleActivityCell.reuseId)
    
    // Register data store
    self.collectionDataStore = ScheduleListCollectionDataStore(self)
    self.collectionView.dataSource = collectionDataStore
    self.collectionView.delegate = collectionDataStore
    
    // Set Collection View layout settings
    let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    flowLayout.sectionInset = UIEdgeInsets(top: 8.0, left: 0.0, bottom: 16.0, right: 0.0) // Add padding to top/bottom of list
    flowLayout.minimumLineSpacing = 6.0 // Item Spacing
  }
  
  /**
   Open a ViewController based on the RosterActivity type
   */
  func triggerOpenActivity(item: RosterActivity) {
    let viewController: UIViewController
    
    switch item.activityType {
    case .work:           viewController = ViewUtil.workDetailsViewController(withActivity: item)
    case .dayOff:         viewController = ViewUtil.dayOffDetailsViewController(withActivity: item)
    case .sleep:          viewController = ViewUtil.sleepDetailsViewController(withActivity: item)
    case .lightExposure:  viewController = ViewUtil.lightExposureDetailsViewController(withActivity: item)
    case .avoidLight:     viewController = ViewUtil.avoidLightDetailsViewController(withActivity: item)
    case .caffeine:       viewController = ViewUtil.caffeineDetailsViewController(withActivity: item)
    default: return
    }
    
    self.navigationController?.pushViewController(viewController, animated: true)
  }

  // MARK: - Button Delegates

  @IBAction func addButtonTapped(_ sender: Any) {
    self.present(ViewUtil.addRosterNavigationController, animated: true, completion: nil)
  }

  @IBAction func menuButtonTapped(_ sender: Any) {
    self.present(ViewUtil.settingsNavigationController, animated: true, completion: nil)
  }

  @IBAction func rosterButtonTapped(_ sender: Any) {
    self.navigationController?.pushViewController(ViewUtil.rosterListViewController, animated: true)
  }

  @IBAction func addRosterButtonTapped(_ sender: Any) {
    self.present(ViewUtil.addRosterNavigationController, animated: true, completion: nil)
  }
}
