//
//  NotificationPreferencesInfoViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays general information related to notifications
 */
final class NotificationPreferencesInfoViewController: UIViewController {
  internal static let storyboardId = "\(NotificationPreferencesInfoViewController.self)"
  
  @IBOutlet weak var doneButton: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.doneButton.standardButtonStyle()
  }

  // MARK: - Button Delegates

  @IBAction func closeButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
    self.dismiss(animated: true, completion: nil)
  }
}
