//
//  NotificationPreferencesViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit
import UserNotifications

/**
 This VC enables the user to select which notification preferences they would like to receive notifications for,
 within the onboarding phase
 */
final class NotificationPreferencesViewController: UIViewController {
  internal static let storyboardId = "\(NotificationPreferencesViewController.self)"

  @IBOutlet weak var overtimeSwitch: UISwitch!
  @IBOutlet weak var sleepSwitch: UISwitch!
  @IBOutlet weak var caffeineSwitch: UISwitch!
  @IBOutlet weak var lightSwitch: UISwitch!
  @IBOutlet weak var getStartedButton: UIButton!
  @IBOutlet weak var moreInfoButton: UIButton!
  
  private let notificationPermissionMessage = "Please consider enabling Notification permission to be provided with roster-based information."

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.getStartedButton.ctaStyle()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .notifications)
  }
  
  /**
   Request Notification Permission, and display message if there was an error or if permission was denied
   */
  private func requestNotificationPermission() {
    let permissions: UNAuthorizationOptions = [.alert, .badge]
    
    UNUserNotificationCenter
      .current()
      .requestAuthorization(options: permissions) { result, error in
        if let error = error {
          print("Error: \(error)")
          let dialog = ViewUtil.createBasicPopupDialog(title: "Notifications Error",
                                                       message: "There was an error requesting Notfication permission",
                                                       handler: { _ in self.completeNotificationPreferences() },
                                                       secondHandler: { _ in ViewUtil.openSettingsApp() },
                                                       secondHandlerTitle: "Open Settings")
          self.present(dialog, animated: true, completion: nil)
          return
        }
        
        if !result {
          let dialog = ViewUtil.createBasicPopupDialog(title: "Notifications Permission",
                                                       message: self.notificationPermissionMessage,
                                                       handler: { _ in self.completeNotificationPreferences() },
                                                       secondHandler: { _ in ViewUtil.openSettingsApp() },
                                                       secondHandlerTitle: "Open Settings")
          self.present(dialog, animated: true, completion: nil)
          return
        }
        
        self.completeNotificationPreferences()
    }
  }
  
  private func completeNotificationPreferences() {
    // ZORAN: This would be a good spot to store the selected notification preference values
    
    self.present(ViewUtil.rosterNavigationController, animated: true, completion: nil)
  }
  
  // MARK: - Button Delegates

  @IBAction func getStartedButtonTapped(_ sender: Any) {
    // Request notification permission if at least one switch is enabled
    if self.overtimeSwitch.isOn || self.sleepSwitch.isOn || self.caffeineSwitch.isOn || self.lightSwitch.isOn {
      self.requestNotificationPermission()
    } else {
      self.completeNotificationPreferences()
    }
  }

  @IBAction func moreInfoButtonTapped(_ sender: Any) {
    self.present(ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.notificationPreferencesInfoViewController),
                 animated: true, completion: nil)
  }
}
