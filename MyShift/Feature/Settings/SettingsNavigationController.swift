//
//  SettingsNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 25/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This Navigation Controller is displayed to contain the Settings VC, with light status bar
 */
final class SettingsNavigationController: UINavigationController {
  internal static let storyboardId = "\(SettingsNavigationController.self)"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setNeedsStatusBarAppearanceUpdate()
    
    self.setViewControllers([ViewUtil.settingsViewController], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
}
