//
//  SettingsViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC enables the user to toggle various app settings, such as Roster Input, notification options, and other
 configuration values from the onboarding stage
 */
final class SettingsViewController: UIViewController {
  internal static let storyboardId = "\(SettingsViewController.self)"

  @IBOutlet weak var rosterInputSegmentedControl: UISegmentedControl!
  @IBOutlet weak var healthDataSwitch: UISwitch!
  @IBOutlet weak var overtimeSwitch: UISwitch!
  @IBOutlet weak var sleepSwitch: UISwitch!
  @IBOutlet weak var caffeineSwitch: UISwitch!
  @IBOutlet weak var lightSwitch: UISwitch!
  
  @IBOutlet weak var stationAddressView: UIView!
  @IBOutlet weak var shiftTimesView: UIView!
  @IBOutlet weak var commuteTimeView: UIView!
  @IBOutlet weak var caffeinePreferenceView: UIView!

  @IBOutlet weak var commuteTimeLabel: UILabel!
  @IBOutlet weak var stationAddressLabel: UILabel!

  @IBOutlet weak var automaticRosterView: UIView!
  @IBOutlet weak var manualRosterView: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureGestures()
    self.refreshSegmentedViews()
    self.loadSavedSettings()
  }
  
  private func configureGestures() {
    // Attach caffeine view tap gesture
    let caffeineTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.caffeinePreferenceViewTapped(_:)))
    self.caffeinePreferenceView.addGestureRecognizer(caffeineTapGesture)
    self.caffeinePreferenceView.isUserInteractionEnabled = true

    // Attach commute view tap gesture
    let commuteTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.commutePreferenceViewTapped(_:)))
    self.commuteTimeView.addGestureRecognizer(commuteTapGesture)
    self.commuteTimeView.isUserInteractionEnabled = true
    
    // Attach shift times view tap gesture
    let shiftTimesTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.shiftTimesPreferenceViewTapped(_:)))
    self.shiftTimesView.addGestureRecognizer(shiftTimesTapGesture)
    self.shiftTimesView.isUserInteractionEnabled = true
    
    // Attach station address view tap gesture
    let stationAddressTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.stationAddressPreferenceViewTapped(_:)))
    self.stationAddressView.addGestureRecognizer(stationAddressTapGesture)
    self.stationAddressView.isUserInteractionEnabled = true
  }
  
  /**
   Toggle roster visibility states
   */
  private func refreshSegmentedViews() {
    switch self.rosterInputSegmentedControl.selectedSegmentIndex {
    case 0:
      self.automaticRosterView.isHidden = false
      self.manualRosterView.isHidden = true
    case 1:
      self.automaticRosterView.isHidden = true
      self.manualRosterView.isHidden = false
    default: return
    }
  }
  
  private func loadSavedSettings() {
    // ZORAN: This would be a good spot to load in the saved configuration settings from the DB, and toggle UI
    // state based on Roster Input
  }
  
  @objc func caffeinePreferenceViewTapped(_ sender: Any) {
    let vc = ViewUtil.settingsCaffeinePreferenceNavigationController(withCallback: { didComplete in
      // Execute necessary callback functionality
    })
    
    self.present(vc, animated: true, completion: nil)
  }
  
  @objc func commutePreferenceViewTapped(_ sender: Any) {
    let vc = ViewUtil.settingsCommutePreferenceNavigationController(withCallback: { didComplete in
      // ZORAN: This would be a good spot to fetch the latest commute time from the DB and update the commute time label
    })
    
    self.present(vc, animated: true, completion: nil)
  }
  
  @objc func shiftTimesPreferenceViewTapped(_ sender: Any) {
    let vc = ViewUtil.settingsShiftTimesPreferenceNavigationController(withCallback: { didComplete in
      // Execute necessary callback functionality
    })
    
    self.present(vc, animated: true, completion: nil)
  }
  
  @objc func stationAddressPreferenceViewTapped(_ sender: Any) {
    let vc = ViewUtil.locationSearchNavigationController { location in
      // ZORAN: This would be a good spot to fetch the latest station address from the DB and update
      // the station address label

      self.stationAddressLabel.text = location.address
    }
    
    self.present(vc, animated: true, completion: nil)
  }
  
  // MARK: - Button Delegates
  
  @IBAction func doneButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }

  @IBAction func rosterInputSegmentedControl(_ sender: Any) {
    self.refreshSegmentedViews()
  }
}
