//
//  SettingsShiftTimesNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 25/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This navigation controller displays only a single child VC to handle updating shift times
 preference settings
 */
final class SettingsShiftTimesPreferenceNavigationController: UINavigationController {
  internal static let storyboardId = "\(SettingsShiftTimesPreferenceNavigationController.self)"
  
  // VC setup parameters
  var completionCallback: ((Bool) -> ())?
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.setNeedsStatusBarAppearanceUpdate()
    
    self.setViewControllers([ViewUtil.addShiftTimesViewController], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  /**
   Dismiss Navigation Controller, and trigger callback based on whether the shift times preference update was successful
   */
  func updateShiftTimesPreferenceCompleted(wasSuccessful: Bool) {
    self.dismiss(animated: true, completion: nil)
    
    self.completionCallback?(wasSuccessful)
  }
}
