//
//  SettingsCaffeinePreferenceNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 25/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This navigation controller displays only a single child VC to handle updating caffeine
 preference settings
 */
final class SettingsCaffeinePreferenceNavigationController: UINavigationController {
  internal static let storyboardId = "\(SettingsCaffeinePreferenceNavigationController.self)"
  
  // VC setup parameters
  var completionCallback: ((Bool) -> ())?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setNeedsStatusBarAppearanceUpdate()
    
    self.setViewControllers([ViewUtil.caffeinePreferenceViewController], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  /**
   Dismiss Navigation Controller, and trigger callback based on whether the caffeine preference update was successful
   */
  func updateCaffeinePreferenceCompleted(wasSuccessful: Bool) {
    self.dismiss(animated: true, completion: nil)
    
    self.completionCallback?(wasSuccessful)
  }
}
