//
//  SettingsCommutePreferenceNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 25/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This navigation controller displays only a single child VC to handle updating commute
 preference settings
 */
final class SettingsCommutePreferenceNavigationController: UINavigationController {
  internal static let storyboardId = "\(SettingsCommutePreferenceNavigationController.self)"
  
  // VC setup parameters
  var completionCallback: ((Bool) -> ())?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setNeedsStatusBarAppearanceUpdate()
    
    self.setViewControllers([ViewUtil.commuteTimeViewController], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  /**
   Dismiss Navigation Controller, and trigger callback based on whether the commute preference update was successful
   */
  func updateCommutePreferenceCompleted(wasSuccessful: Bool) {
    self.dismiss(animated: true, completion: nil)
    
    self.completionCallback?(wasSuccessful)
  }
}
