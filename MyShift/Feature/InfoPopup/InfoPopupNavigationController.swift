//
//  InfoPopupNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 23/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This Navigation Controller displays a child VC for use within a "pop-up" context, with Light status bar styling
 */
final class InfoPopupNavigationController: UINavigationController {
  internal static let storyboardId = "\(InfoPopupNavigationController.self)"
  
  // Child VC to display
  var childVc: UIViewController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setNeedsStatusBarAppearanceUpdate()

    // Dismiss navigation controller if child VC not present
    guard let childVc = self.childVc else {
      self.dismiss(animated: true, completion: nil)
      return
    }

    setViewControllers([childVc], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
}
