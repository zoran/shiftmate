//
//  LightInfoViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays general information related to light, in a list
 */
final class LightInfoViewController: UIViewController {
  internal static let storyboardId = "\(LightInfoViewController.self)"
  
  // MARK: - Button Delegates

  @IBAction func closeBarButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
}
