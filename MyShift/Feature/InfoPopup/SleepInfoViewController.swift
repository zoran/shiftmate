//
//  SleepInfoViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays general information related to sleep, in a list
 */
final class SleepInfoViewController: UIViewController {
  internal static let storyboardId = "\(SleepInfoViewController.self)"
  
  @IBOutlet weak var descriptionLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Apply bold attribute
    if let descriptionText = self.descriptionLabel.text {
      self.descriptionLabel.applyFontWeightStyle(.bold,
                                                 size: 14,
                                                 snippets: ["1. Sleep Before Work:",
                                                            "2. Sleep After Work:"],
                                                 completeString: descriptionText)
    }
  }

  // MARK: - Button Delegates

  @IBAction func closeBarButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
}
