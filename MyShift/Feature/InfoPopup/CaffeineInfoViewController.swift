//
//  CaffeineInfoViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays general information related to caffeine, in a list
 */
final class CaffeineInfoViewController: UIViewController {
  internal static let storyboardId = "\(CaffeineInfoViewController.self)"
  
  // MARK: - Button Delegates
  
  @IBAction func closeButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
}
