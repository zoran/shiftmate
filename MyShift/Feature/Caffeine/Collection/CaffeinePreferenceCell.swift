//
//  CaffeinePreferenceCell.swift
//  MyShift
//
//  Created by Michael Tigas on 16/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom collection cell to denote a Caffeine Preference cell
 */
final class CaffeinePreferenceCell: UICollectionViewCell {
  internal static let reuseId = "\(CaffeinePreferenceCell.self)"

  @IBOutlet weak var preferenceLabel: UILabel!
  @IBOutlet weak var deleteButton: UIButton!

  private var dataStore: CaffeinePreferenceCollectionDataStore?
  private var caffeinePreference: CaffeinePreference?

  /**
   Configure cell values & UI
   */
  internal func configureCell(dataStore: CaffeinePreferenceCollectionDataStore) {
    self.dataStore = dataStore
    
    self.configureUi()
  }
  
  private func configureUi() {
    self.preferenceLabel.layer.masksToBounds = true
    self.preferenceLabel.layer.cornerRadius = 18
  }
  
  /**
   Populate cell values
   */
  internal func populateUi(item: CaffeinePreference) {
    self.resetUi()
    
    self.caffeinePreference = item
    
    self.preferenceLabel.text = "\(item.type.name)"
    self.deleteButton.isHidden = !item.isEditEnabled
  }
  
  private func resetUi() {
    self.caffeinePreference = nil
    self.preferenceLabel.text = ""
  }
  
  func toggleDeleteButtonVisibility(isVisible: Bool) {
    self.deleteButton.isHidden = !isVisible
  }

  @IBAction func deleteButtonTapped(_ sender: Any) {
    self.dataStore?.removeItem(index: self.tag)
  }
}
