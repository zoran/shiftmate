//
//  CaffeinePreferenceViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC enables a user to select their caffeine preference from a predetermined list, accessible via Onboarding
 or Settings screens
 */
final class CaffeinePreferenceViewController: UIViewController {
  internal static let storyboardId = "\(CaffeinePreferenceViewController.self)"

  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var addButton: UIButton!
  @IBOutlet weak var nextButton: UIButton!
  
  // Custom picker
  private var caffeinePreferencePickerView: CaffeinePreferencePickerView?
  private var isCustomPickerDisplayed = false
  
  // Collection
  private var collectionDataStore: CaffeinePreferenceCollectionDataStore?
  private var collectionEditModeEnabled = false

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
    self.createPickerView()
    self.configureCollectionView()
    self.configureNavigationItems()    
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    self.caffeinePreferencePickerView?.hidePickerLayout(viewController: self)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .caffeine)
  }
  
  private func configureUi() {
    self.addButton.standardButtonStyle()
    self.nextButton.ctaStyle()
  }

  /**
   Init CaffeinePreferencePickerView, apply constraints and callbacks
   */
  private func createPickerView() {
    let pickerView = CaffeinePreferencePickerView.instantiateNib()
    self.view.addSubview(pickerView)
    
    pickerView.anchorToBottomOf(viewController: self)

    // Init callbacks
    pickerView.doneCallback = { selectedPreference in
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()

      // Clear all existing items if none selected
      if selectedPreference == .none {
        self.collectionDataStore?.removeAllItems()
      } else {
        self.collectionDataStore?.removeItem(type: .none)
        self.collectionDataStore?.removeItem(type: selectedPreference) // Filter out duplicates
      }
      
      self.collectionDataStore?.addItem(item: CaffeinePreference(type: selectedPreference,
                                                                 isEditEnabled: self.collectionEditModeEnabled))
    }
    
    pickerView.cancelCallback = {
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
    }
    
    pickerView.resetPickerValuesToDefaultState()
    
    self.caffeinePreferencePickerView = pickerView
  }
  
  private func configureCollectionView() {
    // Set transparent background
    self.collectionView.backgroundColor = .clear
    self.collectionView.backgroundView = UIView(frame: .zero)
    
    // Register cells
    self.collectionView.register(UINib(nibName: CaffeinePreferenceCell.reuseId, bundle: nil),
                                 forCellWithReuseIdentifier: CaffeinePreferenceCell.reuseId)
    
    // Register data store
    self.collectionDataStore = CaffeinePreferenceCollectionDataStore(self)
    self.collectionView.dataSource = collectionDataStore
    self.collectionView.delegate = collectionDataStore
    
    // Set Collection View layout settings
    let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    flowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0) // Add padding to top/bottom of list
    flowLayout.minimumLineSpacing = 12.0 // Item Spacing
  }

  /**
   Toggle collection edit state
   */
  private func toggleCollectionEditMode() {
    self.collectionEditModeEnabled = !self.collectionEditModeEnabled
    self.collectionDataStore?.toggleEditVisibility(isEnabled: self.collectionEditModeEnabled)
  }

  /**
   Display specific navigation items based on navigation controller type
   */
  private func configureNavigationItems() {
    guard let navController = self.navigationController as? SettingsCaffeinePreferenceNavigationController else { return }

    // Add close button if nav controller is a settings-based navigation controller
    let closeButton = UIBarButtonItem(image: UIImage(named: "ic_close_navigation"),
                                      style: .plain,
                                      target: self,
                                      action: #selector(self.closeButtonTapped(_:)))
    self.navigationItem.setLeftBarButton(closeButton, animated: false)
  }
  
  @objc func closeButtonTapped(_ sender: Any) {
    if let navController = self.navigationController as? SettingsCaffeinePreferenceNavigationController {
      navController.updateCaffeinePreferenceCompleted(wasSuccessful: false)
    }
  }
  
  /**
   Complete caffeine preference's vc and follow next step based on navigation controller type
   */
  private func triggerForwardNavigation() {
    if let navController = self.navigationController as? OnboardingNavigationController {
      // Display Notification Preferences VC
      navController.pushViewController(ViewUtil.notificationPreferencesViewController, animated: true)
    }
    
    if let navController = self.navigationController as? SettingsCaffeinePreferenceNavigationController {
      navController.updateCaffeinePreferenceCompleted(wasSuccessful: true)
    }
  }

  // MARK: - Button Delegates

  @IBAction func addButtonTapped(_ sender: Any) {
    self.caffeinePreferencePickerView?.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                                                  completionCallback: { self.isCustomPickerDisplayed = $0 })
  }

  @IBAction func nextButtonTapped(_ sender: Any) {
    guard let dataStore = self.collectionDataStore else { return }

    if !dataStore.hasItems() {
      let dialog = ViewUtil.createBasicPopupDialog(title: "Caffeine Preference required",
                                                   message: "myShift requires at least one caffeine preference to personalise your recommended schedule", handler: nil)
      
      self.present(dialog, animated: true, completion: nil)
      return
    }
    
    // ZORAN: This would be a great spot to persist the user selected Caffeine Preference's

    self.triggerForwardNavigation()
  }

  @IBAction func editButtonTapped(_ sender: Any) {
    self.toggleCollectionEditMode()
  }
}
