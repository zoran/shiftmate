//
//  DisableSelectionTextView.swift
//  MyShift
//
//  Created by Michael Tigas on 17/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 Custom TextView which prevents text focus, but maintain hyperlink selection capability
 */
final class DisableSelectionTextView: UITextView {
  
  override init(frame: CGRect, textContainer: NSTextContainer?) {
    super.init(frame: frame, textContainer: textContainer)
    
    self.isEditable = false
    self.isSelectable = true

    self.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    self.isEditable = false
    self.isSelectable = true
    
    self.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
  }

  /**
   Disable ability to perform contextual actions
   */
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    return false
  }
  
  /**
   Do not generate a Caret icon
   */
  override func caretRect(for position: UITextPosition) -> CGRect {
    return CGRect.zero
  }
  
  /**
   Prevent text range selection
   */
  override var selectedTextRange: UITextRange? {
    get { return nil }
    set { return }
  }
}
