//
//  TimeDurationPickerView.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom UI picker that handles selecting a time duration
 */
final class TimeDurationPickerView: UIView {
  @IBOutlet weak var datePickerView: UIDatePicker!
  
  internal var cancelCallback: (() -> ())?
  internal var doneCallback: ((_ durationSelected: TimeInterval) -> ())?

  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUi()
  }
  
  private func prepareUi() {
    self.datePickerView.datePickerMode = .countDownTimer
    self.datePickerView.locale = Locale.init(identifier: "en_GB")
    self.datePickerView.minuteInterval = 1
    self.datePickerView.setValue(UIColor.white, forKey: "textColor")
  }
  
  /**
   Public picker accessor
   */
  func getPickerView() -> UIDatePicker {
    return self.datePickerView
  }
  
  /**
   Set picker values to display 0 hours, 45 minutes
   */
  func resetPickerValuesToDefaultState() {
    self.datePickerView.countDownDuration = 60 * 45
  }

  /**
   Instantiate a Nib of this custom view class
   */
  static func instantiateNib() -> TimeDurationPickerView {
    let nib = UINib(nibName: "\(TimeDurationPickerView.self)", bundle: nil)
    return nib.instantiate(withOwner: self, options: nil).first as! TimeDurationPickerView
  }
  
  /**
   Anchor view constraints to the bottom of the current UIViewController
   */
  func anchorToBottomOf(viewController: UIViewController) {
    self.heightAnchor.constraint(equalToConstant: 260).isActive = true
    self.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor).isActive = true
    self.leadingAnchor.constraint(equalTo: viewController.view.leadingAnchor).isActive = true
    self.trailingAnchor.constraint(equalTo: viewController.view.trailingAnchor).isActive = true
    self.translatesAutoresizingMaskIntoConstraints = false
  }
  
  /**
   Move the frame's layout to the lower portion of the current UIViewController's, a.k.a on-screen
   */
  func showPickerLayout(viewController: UIViewController) {
    var updatedPickerFrame = self.frame
    updatedPickerFrame.origin.y = viewController.view.frame.size.height - self.frame.size.height
    
    self.frame = updatedPickerFrame
  }
  
  /**
   Move the frame's layout to below current UIViewController's height, a.k.a off-screen
   */
  func hidePickerLayout(viewController: UIViewController) {
    var updatedPickerFrame = self.frame
    updatedPickerFrame.origin.y = viewController.view.frame.size.height
    
    self.frame = updatedPickerFrame
  }
  
  /**
   Animate picker visibility
   */
  func togglePickerViewVisibility(_ viewController: UIViewController,
                                  isPickerVisible: Bool,
                                  completionCallback: @escaping (Bool) -> ()) {
    
    UIView.animate(withDuration: 0.20, animations: {
      if isPickerVisible {
        self.hidePickerLayout(viewController: viewController)
      } else {
        self.showPickerLayout(viewController: viewController)
      }
    }, completion: { _ in completionCallback(!isPickerVisible) })
  }
  
  // MARK: - Button Delegates
  
  @IBAction func cancelButtonTapped(_ sender: Any) {
    self.cancelCallback?()
  }
  
  @IBAction func doneButtonTapped(_ sender: Any) {
    self.doneCallback?(self.datePickerView.countDownDuration)
  }
}
