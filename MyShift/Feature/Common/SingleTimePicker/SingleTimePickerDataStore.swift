//
//  SingleTimePickerDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 21/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of time items and handles picker styling
 */
final class SingleTimePickerDataStore: NSObject {
  let thirteenHourTimeArray =
    ["03:00", "04:00", "05:00", "06:00", "07:00",
     "08:00", "09:00", "10:00", "11:00", "12:00",
     "13:00"]

}

// MARK: - UIPickerViewDelegate
extension SingleTimePickerDataStore: UIPickerViewDelegate {
  /**
   Set label text colour
   */
  func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
    let attributedString = NSAttributedString(string: self.thirteenHourTimeArray[row],
                                              attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
    return attributedString
    
  }
}

// MARK: - UIPickerViewDataSource
extension SingleTimePickerDataStore: UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.thirteenHourTimeArray.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return self.thirteenHourTimeArray[row]
  }
}
