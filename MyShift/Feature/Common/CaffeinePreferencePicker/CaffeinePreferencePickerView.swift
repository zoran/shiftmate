//
//  CaffeinePreferencePickerView.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom UI picker that handles selecting a single caffeine item
 */
final class CaffeinePreferencePickerView: UIView {
  @IBOutlet weak var pickerView: UIPickerView!
  
  internal var cancelCallback: (() -> ())?
  internal var doneCallback: ((_ selectedPreference: CaffeinePreference.CaffeineType) -> ())?

  private let pickerDataStore = CaffeinePreferencePickerDataStore()

  override func awakeFromNib() {
    super.awakeFromNib()

    self.prepareUi()
  }
  
  private func prepareUi() {
    self.pickerView.dataSource = pickerDataStore
    self.pickerView.delegate = pickerDataStore
  }
  
  /**
   Public picker accessor
   */
  func getPickerView() -> UIPickerView {
    return self.pickerView
  }
  
  /**
   Set picker values to display the first caffeine option
   */
  func resetPickerValuesToDefaultState() {
    self.pickerView.selectRow(0, inComponent: 0, animated: false)
  }

  /**
   Anchor view constraints to the bottom of the current UIViewController
   */
  func anchorToBottomOf(viewController: UIViewController) {
    self.heightAnchor.constraint(equalToConstant: 260).isActive = true
    self.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor).isActive = true
    self.leadingAnchor.constraint(equalTo: viewController.view.leadingAnchor).isActive = true
    self.trailingAnchor.constraint(equalTo: viewController.view.trailingAnchor).isActive = true
    self.translatesAutoresizingMaskIntoConstraints = false
  }
  
  /**
   Move the frame's layout to the lower portion of the current UIViewController's, a.k.a on-screen
   */
  func showPickerLayout(viewController: UIViewController) {
    var updatedPickerFrame = self.frame
    updatedPickerFrame.origin.y = viewController.view.frame.size.height - self.frame.size.height
    
    self.frame = updatedPickerFrame
  }
  
  /**
   Move the frame's layout to below current UIViewController's height, a.k.a off-screen
   */
  func hidePickerLayout(viewController: UIViewController) {
    var updatedPickerFrame = self.frame
    updatedPickerFrame.origin.y = viewController.view.frame.size.height
    
    self.frame = updatedPickerFrame
  }
  
  /**
   Animate picker visibility
   */
  func togglePickerViewVisibility(_ viewController: UIViewController,
                                  isPickerVisible: Bool,
                                  completionCallback: @escaping (Bool) -> ()) {
    
    UIView.animate(withDuration: 0.20, animations: {
      if isPickerVisible {
        self.hidePickerLayout(viewController: viewController)
      } else {
        self.showPickerLayout(viewController: viewController)
      }
    }, completion: { _ in completionCallback(!isPickerVisible) })
  }

  /**
   Instantiate a Nib of this custom view class
   */
  static func instantiateNib() -> CaffeinePreferencePickerView {
    let nib = UINib(nibName: "\(CaffeinePreferencePickerView.self)", bundle: nil)
    return nib.instantiate(withOwner: self, options: nil).first as! CaffeinePreferencePickerView
  }
  
  // MARK: - Button Delegates
  
  @IBAction func doneButtonTapped(_ sender: Any) {
    let selectedPosition = self.pickerView.selectedRow(inComponent: 0)
    let selectedPreference = self.pickerDataStore.caffeinePreferenceList[selectedPosition]
    
    self.doneCallback?(selectedPreference)
  }
  
  @IBAction func cancelButtonTapped(_ sender: Any) {
    self.cancelCallback?()
  }
}
