//
//  CaffeinePreferencePickerDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of caffeine items and handles picker styling
 */
final class CaffeinePreferencePickerDataStore: NSObject {
  let caffeinePreferenceList: [CaffeinePreference.CaffeineType]
    = [.none, .tea, .coffee, .energyDrink, .cocoaDrink, .colaDrink, .mountainDew, .other]
  
}

// MARK: - UIPickerViewDelegate
extension CaffeinePreferencePickerDataStore: UIPickerViewDelegate {
  /**
   Set label text colour
   */
  func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
    let attributedString = NSAttributedString(string: self.caffeinePreferenceList[row].name,
                                              attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
    return attributedString
    
  }
}

// MARK: - UIPickerViewDataSource
extension CaffeinePreferencePickerDataStore: UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.caffeinePreferenceList.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return self.caffeinePreferenceList[row].name
  }
}
