//
//  TimeRangePickerDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of time items and handles picker styling
 */
final class TimeRangePickerDataStore: NSObject {
  let twentyFourHourTimeArray =
    ["00:00", "01:00", "02:00", "03:00", "04:00",
     "05:00", "06:00", "07:00", "08:00", "09:00",
     "10:00", "11:00", "12:00", "13:00", "14:00",
     "15:00", "16:00", "17:00", "18:00", "19:00",
     "20:00", "21:00", "22:00", "23:00"]
}

// MARK: - UIPickerViewDelegate
extension TimeRangePickerDataStore: UIPickerViewDelegate {
  /**
   Validate picker adjustment to prevent duplicate row selection
   */
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    let adjustedComponent: Int
    let oppositeComponent: Int
    
    if component == 0 {
      adjustedComponent = 0
      oppositeComponent = 1
    } else {
      adjustedComponent = 1
      oppositeComponent = 0
    }
    
    let selectedRowInAdjustedComponent = pickerView.selectedRow(inComponent: adjustedComponent)
    let selectedRowInOppositeComponent = pickerView.selectedRow(inComponent: oppositeComponent)
    
    // If rows match, alternate adjusted component to a different value
    if selectedRowInAdjustedComponent == selectedRowInOppositeComponent {
      // If both components are set to the very first row, move one row forward instead of back
      let newRow = selectedRowInAdjustedComponent == 0 ? 1 : selectedRowInAdjustedComponent - 1
      
      pickerView.selectRow(newRow, inComponent: adjustedComponent, animated: true)
    }
  }
  
  /**
   Set label text colour
   */
  func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
    let attributedString = NSAttributedString(string: self.twentyFourHourTimeArray[row],
                                              attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
    return attributedString
    
  }
}

// MARK: - UIPickerViewDataSource
extension TimeRangePickerDataStore: UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 2
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.twentyFourHourTimeArray.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return self.twentyFourHourTimeArray[row]
  }
}
