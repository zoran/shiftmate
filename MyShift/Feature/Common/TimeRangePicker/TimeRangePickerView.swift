//
//  TimeRangePickerView.swift
//  MyShift
//
//  Created by Michael Tigas on 19/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom UI picker that handles selecting a time range
 */
final class TimeRangePickerView: UIView {
  @IBOutlet weak var pickerView: UIPickerView!
  @IBOutlet weak var finishLabel: UILabel!
  @IBOutlet weak var startLabel: UILabel!

  internal var cancelCallback: (() -> ())?
  internal var doneCallback: ((_ startTime: String, _ endTime: String) -> ())?
  
  private let pickerDataStore = TimeRangePickerDataStore()

  override func awakeFromNib() {
    super.awakeFromNib()

    self.prepareUi()
  }
  
  private func prepareUi() {
    // Add label shadows
    self.startLabel.layer.masksToBounds = false
    self.startLabel.layer.shadowRadius = 5
    self.startLabel.layer.shadowOpacity = 1
    self.startLabel.layer.shadowOffset = CGSize(width: 1, height: 2)

    self.finishLabel.layer.masksToBounds = false
    self.finishLabel.layer.shadowRadius = 5
    self.finishLabel.layer.shadowOpacity = 1
    self.finishLabel.layer.shadowOffset = CGSize(width: 1, height: 2)
    
    self.pickerView.dataSource = pickerDataStore
    self.pickerView.delegate = pickerDataStore
  }
  
  /**
   Public picker accessor
   */
  func getPickerView() -> UIPickerView {
    return self.pickerView
  }
  
  /**
   Set picker values to display 07:00 and 15:00
   */
  func resetPickerValuesToDefaultState() {
    self.pickerView.selectRow(7, inComponent: 0, animated: false)
    self.pickerView.selectRow(15, inComponent: 1, animated: false)
  }

  /**
   Anchor view constraints to the bottom of the current UIViewController
   */
  func anchorToBottomOf(viewController: UIViewController) {
    self.heightAnchor.constraint(equalToConstant: 260).isActive = true
    self.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor).isActive = true
    self.leadingAnchor.constraint(equalTo: viewController.view.leadingAnchor).isActive = true
    self.trailingAnchor.constraint(equalTo: viewController.view.trailingAnchor).isActive = true
    self.translatesAutoresizingMaskIntoConstraints = false
  }
  
  /**
   Move the frame's layout to the lower portion of the current UIViewController's, a.k.a on-screen
   */
  func showPickerLayout(viewController: UIViewController) {
    var updatedPickerFrame = self.frame
    updatedPickerFrame.origin.y = viewController.view.frame.size.height - self.frame.size.height
    
    self.frame = updatedPickerFrame
  }
  
  /**
   Move the frame's layout to below current UIViewController's height, a.k.a off-screen
   */
  func hidePickerLayout(viewController: UIViewController) {
    var updatedPickerFrame = self.frame
    updatedPickerFrame.origin.y = viewController.view.frame.size.height
    
    self.frame = updatedPickerFrame
  }
  
  /**
   Animate picker visibility
   */
  func togglePickerViewVisibility(_ viewController: UIViewController,
                                  isPickerVisible: Bool,
                                  completionCallback: @escaping (Bool) -> ()) {
    
    UIView.animate(withDuration: 0.20, animations: {
      if isPickerVisible {
        self.hidePickerLayout(viewController: viewController)
      } else {
        self.showPickerLayout(viewController: viewController)
      }
    }, completion: { _ in completionCallback(!isPickerVisible) })
  }
  
  /**
   Instantiate a Nib of this custom view class
   */
  static func instantiateNib() -> TimeRangePickerView {
    let nib = UINib(nibName: "\(TimeRangePickerView.self)", bundle: nil)
    return nib.instantiate(withOwner: self, options: nil).first as! TimeRangePickerView
  }
  
  // MARK: - Button Delegates

  @IBAction func cancelButtonTapped(_ sender: Any) {
    self.cancelCallback?()
  }
  
  @IBAction func doneButtonTapped(_ sender: Any) {
    let startTimePosition = self.pickerView.selectedRow(inComponent: 0)
    let endTimePosition = self.pickerView.selectedRow(inComponent: 1)
    
    let startTimeString = self.pickerDataStore.twentyFourHourTimeArray[startTimePosition]
    let endTimeString = self.pickerDataStore.twentyFourHourTimeArray[endTimePosition]
    
    self.doneCallback?(startTimeString, endTimeString)
  }
}
