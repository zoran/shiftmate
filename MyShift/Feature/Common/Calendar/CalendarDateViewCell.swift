//
//  CalendarDateViewCell.swift
//  MyShift
//
//  Created by Michael Tigas on 24/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit
import FSCalendar

/**
 A custom styled Calendar Cell that handles various selection types to display alternate selection shapes
 */
class CalendarDateViewCell: FSCalendarCell {
  static let reuseId: String = "\(CalendarDateViewCell.self)"
  
  weak var selectionLayer: CAShapeLayer!
  var customTitle: UILabel? // Custom centred UILabel to sit in centre of cell
  
  var selectionType: SelectionType = .none {
    didSet {
      setNeedsLayout()
    }
  }
  
  // Type of Cell selection type
  enum SelectionType : Int {
    case none
    case single
    case leftBorder
    case middle
    case rightBorder
  }
  
  required init!(coder aDecoder: NSCoder!) {
    super.init(coder: aDecoder)
    self.configureUi()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.configureUi()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()

    self.refreshDateCellView()
  }
  
  private func configureUi() {
    // Create a custom shape layer
    let selectionLayer = CAShapeLayer()
    selectionLayer.fillColor = UIColor(red: 13/255.0, green: 74/255.0, blue: 147/255.0, alpha: 255/255.0).cgColor
    selectionLayer.actions = ["hidden": NSNull()]
    
    self.contentView.layer.insertSublayer(selectionLayer, below: self.titleLabel!.layer)
    self.selectionLayer = selectionLayer
    
    // Set a clear background view
    let view = UIView(frame: self.bounds)
    view.backgroundColor = .clear
    self.backgroundView = view;
    
    // Create a custom title
    let title = UILabel(frame: CGRect(x: 0, y: 0,
                                      width: view.bounds.width,
                                      height: view.bounds.height))
    self.addSubview(title)
    self.customTitle = title
    
    self.shapeLayer.isHidden = true // Hide Calendar Cell shape layer
  }
  
  /**
   Format custom title and center view in parent content view
   */
  func applyTitleFormatting() {
    // Apply title styling
    self.customTitle?.textColor = .white
    self.customTitle?.font = UIFont.systemFont(ofSize: 13)
    self.customTitle?.textAlignment = .center
    
    // Apply title constraints
    self.customTitle?.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
    self.customTitle?.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
    self.customTitle?.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
    self.customTitle?.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
    self.customTitle?.translatesAutoresizingMaskIntoConstraints = false
  }
  
  /**
   Re-apply updated date title, and selection layer path based on updated selection type
   */
  private func refreshDateCellView() {
    self.backgroundView?.frame = self.bounds.insetBy(dx: 1, dy: 1)
    
    self.selectionLayer.frame = CGRect(x: 0, y: 0,
                                       width: self.contentView.bounds.width,
                                       height: self.contentView.bounds.height)
    
    // Set custom title label to match hidden title label
    if let titleText = self.titleLabel.text { self.customTitle?.text = titleText }
    
    switch selectionType {
    case .single:       self.applySingleSelectionLayerPath()
    case .middle:       self.applyMiddleSelectionLayerPath()
    case .leftBorder:   self.applyLeftBorderSelectionLayerPath()
    case .rightBorder:  self.applyRightBorderSelectionLayerPath()
    default: return
    }
    
    setNeedsUpdateConstraints()
  }
  
  private func applySingleSelectionLayerPath() {
    let verticalPadding: CGFloat = 4.0
    let horizontalPadding: CGFloat = 8.0
    
    let paddedBounds = CGRect(x: horizontalPadding,
                              y: verticalPadding,
                              width: self.selectionLayer.bounds.width - (horizontalPadding * 2),
                              height: self.selectionLayer.bounds.height - (verticalPadding * 2))
    
    self.selectionLayer.path = UIBezierPath(roundedRect: paddedBounds,
                                            byRoundingCorners: [.topLeft, .bottomLeft, .topRight, .bottomRight],
                                            cornerRadii: CGSize(width: paddedBounds.width / 2,
                                                                height: paddedBounds.width / 2)).cgPath
  }
  
  private func applyMiddleSelectionLayerPath() {
    let verticalPadding: CGFloat = 4.0
    let horizontalPadding: CGFloat = 0.0
    
    let paddedBounds = CGRect(x: horizontalPadding,
                              y: verticalPadding,
                              width: self.selectionLayer.bounds.width,
                              height: self.selectionLayer.bounds.height - (verticalPadding * 2))
    self.selectionLayer.path = UIBezierPath(rect: paddedBounds).cgPath
  }
  
  private func applyLeftBorderSelectionLayerPath() {
    let verticalPadding: CGFloat = 4.0
    let horizontalPadding: CGFloat = 8.0
    
    let paddedBounds = CGRect(x: horizontalPadding,
                              y: verticalPadding,
                              width: self.selectionLayer.bounds.width,
                              height: self.selectionLayer.bounds.height - (verticalPadding * 2))
    
    self.selectionLayer.path = UIBezierPath(roundedRect: paddedBounds,
                                            byRoundingCorners: [.topLeft, .bottomLeft],
                                            cornerRadii: CGSize(width: paddedBounds.width / 2,
                                                                height: paddedBounds.width / 2)).cgPath
  }
  
  private func applyRightBorderSelectionLayerPath() {
    let verticalPadding: CGFloat = 4.0
    let horizontalPadding: CGFloat = 0.0
    
    let paddedBounds = CGRect(x: horizontalPadding,
                              y: verticalPadding,
                              width: self.selectionLayer.bounds.width - (verticalPadding * 2),
                              height: self.selectionLayer.bounds.height - (verticalPadding * 2))
    
    self.selectionLayer.path = UIBezierPath(roundedRect: paddedBounds,
                                            byRoundingCorners: [.topRight, .bottomRight],
                                            cornerRadii: CGSize(width: paddedBounds.width / 2,
                                                                height: paddedBounds.width / 2)).cgPath
  }
}
