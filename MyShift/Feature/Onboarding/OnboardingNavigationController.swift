//
//  OnboardingNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 16/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This Navigation Controller is displayed for new install users which will proceed them through the subsequent
 Onboarding process
 */
final class OnboardingNavigationController: UINavigationController {
  internal static let storyboardId = "\(OnboardingNavigationController.self)"
  
  private var onboardingPageControl: UIPageControl?
  
  // Simplify page control value selection when navigating through onboarding screen flow
  enum OnboardingPageType {
    case start, yourWorkRoster, rosterInput,
    shiftTimesLocation, commute, caffeine,
    notifications
    
    var pagePosition: Int {
      switch self {
      case .start:              return 0
      case .yourWorkRoster:     return 1
      case .rosterInput:        return 2
      case .shiftTimesLocation: return 3
      case .commute:            return 4
      case .caffeine:           return 5
      case .notifications:      return 6
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    self.setNeedsStatusBarAppearanceUpdate()
    self.delegate = self
    self.useCustomBackIndicator()
    self.attachPageControl()

    self.setViewControllers([ViewUtil.onboardingStartViewController], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  /**
   Display a Page Control at the top of the Navigation Controller
   */
  private func attachPageControl() {
    let pageControl = UIPageControl(frame: self.navigationBar.bounds)
    pageControl.isUserInteractionEnabled = false // Prevent click capture
    
    // Start, Your WR, Select Roster, Shift Times/Shift Location, Commute, Caffeine, Notifications
    pageControl.numberOfPages = 7
    pageControl.currentPage = 0
    
    self.navigationBar.addSubview(pageControl)

    self.onboardingPageControl = pageControl
  }
  
  /**
   Update page control position
   */
  func updatePageControl(page: OnboardingPageType) {
    self.onboardingPageControl?.currentPage = page.pagePosition
  }
}

// MARK: - UINavigationControllerDelegate
extension OnboardingNavigationController: UINavigationControllerDelegate {
  func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    // Do not display a VC title
    // This will also prevent previous VC title being shown in Back button
    viewController.title = " "
  }
}
