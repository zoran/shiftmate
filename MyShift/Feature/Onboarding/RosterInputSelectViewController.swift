//
//  RosterInputSelectViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation

/**
 This VC enables a user to select a roster input method: Automatic or Manual, and requests the necessary permissions
 if Automatic is selected
 */
final class RosterInputSelectViewController: UIViewController {
  internal static let storyboardId = "\(RosterInputSelectViewController.self)"

  @IBOutlet weak var manualButton: UIButton!
  @IBOutlet weak var automaticButton: UIButton!
  @IBOutlet weak var moreInfoButton: UIButton!
  
  private var locationManager: CLLocationManager?
  private let locationServicesMessage = "To access the Automatic roster input feature, please enable Location Services for your device. Alternatively, you can select the Manual roster input option."
  private let notificationPermissionMessage = "To access the Automatic roster Input feature, please enable Notification permission for myShift. Alternatively, you can select the Manual roster input option."
  private let locationPermissionMessage = "To access the Automatic roster Input feature, please enable Location access for when myShift is not being used. Alternatively, you can select the Manual roster input option."
  
  var isLocationServicesEnabled: Bool { get { return CLLocationManager.locationServicesEnabled() }  }
  var locationManagerAuthStatus: CLAuthorizationStatus { get { return CLLocationManager.authorizationStatus() }  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.automaticButton.standardButtonStyle()
    self.manualButton.standardButtonStyle()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .rosterInput)
  }
  
  /**
   Check for Location Services state, and initialise LocationManager. The location manager delegate will continue
   with location permission validation
   */
  private func triggerAutomaticRosterSetup() {
    if !self.isLocationServicesEnabled {
      let dialog = ViewUtil.createBasicPopupDialog(title: "Location Services recommended",
                                                   message: self.locationServicesMessage,
                                                   handler: nil)
      self.present(dialog, animated: true, completion: nil)
      return
    }
    
    // Initialising location manager and attaching the delegate will immediately trigger the didChangeAuthorization
    // callback function
    self.locationManager = CLLocationManager()
    self.locationManager?.delegate = self
  }
  
  /**
   Request Notification Permission, and display message if there was an error or if permission was denied
   */
  private func requestNotificationPermission() {
    let permissions: UNAuthorizationOptions = [.alert, .badge]
    
    UNUserNotificationCenter
      .current()
      .requestAuthorization(options: permissions) { result, error in
        if let error = error {
          let dialog = ViewUtil.createBasicPopupDialog(title: "Notifications Error",
                                                       message: "There was an error requesting Notfication permission",
                                                       handler: { _ in },
                                                       secondHandler: { _ in ViewUtil.openSettingsApp() },
                                                       secondHandlerTitle: "Open Settings")
          self.present(dialog, animated: true, completion: nil)
          
          print(error)
          return
        }
        
        if !result {
          let dialog = ViewUtil.createBasicPopupDialog(title: "Notification Permission",
                                                       message: self.notificationPermissionMessage,
                                                       handler: { _ in },
                                                       secondHandler: { _ in ViewUtil.openSettingsApp() },
                                                       secondHandlerTitle: "Open Settings")
          self.present(dialog, animated: true, completion: nil)
        }
        
        self.automaticRosterPermissionsSuccessful()
      }
  }

  /**
   Request Location Permission if not previously requested
   */
  private func requestLocationPermission() {
    switch self.locationManagerAuthStatus {
    case .notDetermined:
      self.locationManager?.requestAlwaysAuthorization() // Request Location Permission
    default:
      let dialog = ViewUtil.createBasicPopupDialog(title: "Location Permission",
                                                   message: self.locationPermissionMessage,
                                                   handler: { _ in },
                                                   secondHandler: { _ in ViewUtil.openSettingsApp() },
                                                   secondHandlerTitle: "Open Settings")
      self.present(dialog, animated: true, completion: nil)
    }
  }
  
  /**
   Push the station address VC
   */
  private func automaticRosterPermissionsSuccessful() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
      self.navigationController?.pushViewController(ViewUtil.stationAddressViewController, animated: true)
    }
  }
  
  /**
   Push the Add Shift Times vc
   */
  private func triggerManualRosterSetup() {
    self.navigationController?.pushViewController(ViewUtil.addShiftTimesViewController, animated: true)
  }
  
  // MARK: - Button Delegates

  @IBAction func manualButtonTapped(_ sender: Any) {
    self.triggerManualRosterSetup()
  }

  @IBAction func automaticButtonTapped(_ sender: Any) {
    self.triggerAutomaticRosterSetup()
  }
  
  @IBAction func moreInfoButtonTapped(_ sender: Any) {
    self.present(ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.rosterInputInfoViewController), animated: true, completion: nil)
  }
}

// MARK: - CLLocationManagerDelegate
extension RosterInputSelectViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    switch status {
    case .authorizedAlways:
      self.requestNotificationPermission()
    default:
      self.requestLocationPermission()
    }
  }
}
