//
//  YourWorkRosterViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a hyperlinked label which user can tap to view more information on a specific topic
 */
final class YourWorkRosterViewController: UIViewController {
  internal static let storyboardId = "\(YourWorkRosterViewController.self)"

  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var descriptionTextLabel: UITextView!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.nextButton.ctaStyle()
    self.attachLabelStylingAndInteractionLinks()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .yourWorkRoster)
  }

  /**
   Apply an underlined style to specific words, and interaction delegate
   */
  private func attachLabelStylingAndInteractionLinks() {
    let word1 = "sleep"
    let word2 = "light"
    let word3 = "caffeine"
    
    let underlineAttributeString = NSMutableAttributedString(string: self.descriptionTextLabel.text)
    
    // Determine character range from existing label content
    let range1 = (self.descriptionTextLabel.text as NSString).range(of: word1)
    let range2 = (self.descriptionTextLabel.text as NSString).range(of: word2)
    let range3 = (self.descriptionTextLabel.text as NSString).range(of: word3)
    
    // Add text underline attributes to matching word and range
    underlineAttributeString.addAttributes([
      .underlineStyle : NSUnderlineStyle.styleSingle.rawValue,
      .link : word1
      ], range: range1)
    underlineAttributeString.addAttributes([
      .underlineStyle : NSUnderlineStyle.styleSingle.rawValue,
      .link : word2
      ], range: range2)
    underlineAttributeString.addAttributes([
      .underlineStyle : NSUnderlineStyle.styleSingle.rawValue,
      .link : word3
      ], range: range3)
    
    // Color remaining text White and set font size
    underlineAttributeString.addAttributes([
      .foregroundColor: UIColor.white,
      NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)
      ], range: NSRange(location: 0, length: self.descriptionTextLabel.text.count))
    
    // Apply attributes
    self.descriptionTextLabel.attributedText = underlineAttributeString
    self.descriptionTextLabel.textAlignment = .center
    self.descriptionTextLabel.delegate = self
  }
  
  private func displayTextLinkedViewController(textValue: String) {
    // Display a view controller based on tapped URL/Text string
    let viewController: UIViewController
    switch textValue {
    case "sleep":
      viewController = ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.sleepInfoViewController)
    case "light":
      viewController = ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.lightInfoViewController)
    case "caffeine":
      viewController = ViewUtil.infoPopupNavigationController(withViewController: ViewUtil.caffeineInfoViewController)
    default: return
    }
    
    self.present(viewController, animated: true, completion: nil)
  }
  
  // MARK: - Button Delegates

  @IBAction func nextButtonTapped(_ sender: Any) {
    self.navigationController?.pushViewController(ViewUtil.rosterInputSelectViewController, animated: true)
  }
}

// MARK: - UITextViewDelegate
extension YourWorkRosterViewController: UITextViewDelegate {
  func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
    self.displayTextLinkedViewController(textValue: URL.absoluteString)

    return true
  }
}

