//
//  RosterInputInfoViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays general information related to your roster
 */
final class RosterInputInfoViewController: UIViewController {
  internal static let storyboardId = "\(RosterInputInfoViewController.self)"

  @IBOutlet weak var doneButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.doneButton.standardButtonStyle()
  }
  
  private func closeVc() {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
  
  // MARK: - Button Delegates

  @IBAction func closeBarButtonTapped(_ sender: Any) {
    self.closeVc()
  }

  @IBAction func doneButtonTapped(_ sender: Any) {
    self.closeVc()
  }
}
