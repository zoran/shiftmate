//
//  OnboardingStartViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 The first displayed onboarding welcome view controller
 */
final class OnboardingStartViewController: UIViewController {
  internal static let storyboardId = "\(OnboardingStartViewController.self)"

  @IBOutlet weak var nextButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.nextButton.ctaStyle()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .start)
  }
  
  // MARK: - Button Delegates

  @IBAction func nextButtonTap(_ sender: Any) {
    self.navigationController?.pushViewController(ViewUtil.yourWorkRosterViewController, animated: true)
  }
}
