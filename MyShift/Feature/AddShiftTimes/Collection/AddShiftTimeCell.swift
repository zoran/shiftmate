//
//  AddShiftTimeCell.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom collection cell to denote an Add Shift Time cell
 */
final class AddShiftTimeCell: UICollectionViewCell {
  internal static let reuseId = "\(AddShiftTimeCell.self)"

  @IBOutlet weak var deleteButton: UIButton!
  @IBOutlet weak var timeLabel: UILabel!

  private var dataStore: AddShiftTimesCollectionDataStore?
  private var shiftTimeRange: ShiftTimeRange?

  /**
   Configure cell values & UI
   */
  internal func configureCell(dataStore: AddShiftTimesCollectionDataStore) {
    self.dataStore = dataStore
    
    self.configureUi()
  }
  
  private func configureUi() {
    self.timeLabel.layer.masksToBounds = true
    self.timeLabel.layer.cornerRadius = 18
  }
  
  /**
   Populate cell values
   */
  internal func populateUi(item: ShiftTimeRange) {
    self.resetUi()
    
    self.shiftTimeRange = item
    
    self.timeLabel.text = "\(item.startTime) - \(item.endTime)"
    self.deleteButton.isHidden = !item.isEditEnabled
  }
  
  private func resetUi() {
    self.shiftTimeRange = nil
    self.timeLabel.text = ""
  }

  func toggleDeleteButtonVisibility(isVisible: Bool) {
    self.deleteButton.isHidden = !isVisible
  }
  
  @IBAction func deleteButtonTapped(_ sender: Any) {
    self.dataStore?.removeItem(index: self.tag)
  }
}
