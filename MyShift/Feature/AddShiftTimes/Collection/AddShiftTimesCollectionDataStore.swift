//
//  AddShiftTimesCollectionDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of item's, handles cell styling and list modification/updates
 */
final class AddShiftTimesCollectionDataStore: NSObject {
  fileprivate var items = [ShiftTimeRange]()
  private var parentController: AddShiftTimesViewController?
  
  init(_ controller: AddShiftTimesViewController) {
    super.init()
    self.parentController = controller
  }

  /**
   Add item to collection data store, and refresh collection view
   */
  func addItem(item: ShiftTimeRange) {
    // Prevent adding item duplicates
    for savedItem in self.items {
      if savedItem.startTime == item.startTime && savedItem.endTime == item.endTime {
        return
      }
    }

    self.items.append(item)
    self.parentController?.collectionView.reloadData()
  }
  
  /**
   Remove item from collection data store, and refresh collection view
   */
  func removeItem(itemPosition: Int) {
    self.items.remove(at: itemPosition)
    self.parentController?.collectionView.reloadData()
  }
 
  /**
   Remove item based on index position
   */
  func removeItem(index: Int) {
    self.items.remove(at: index)
    self.parentController?.collectionView.reloadData()
  }
  
  func hasItems() -> Bool {
    return !self.items.isEmpty
  }
  
  /**
   Update edit state variable for collection list model's, and refresh collection view
   */
  func toggleEditVisibility(isEnabled: Bool) {
    for (index, _) in self.items.enumerated() {
      self.items[index].toggleEditState()
    }
    
    self.parentController?.collectionView.reloadData()
  }
}

// MARK: - UICollectionViewDataSource
extension AddShiftTimesCollectionDataStore: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AddShiftTimeCell.reuseId, for: indexPath) as! AddShiftTimeCell
    
    cell.tag = indexPath.item // Set cell tag as node position
    cell.configureCell(dataStore: self)
    cell.populateUi(item: items[indexPath.item])

    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.items.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension AddShiftTimesCollectionDataStore: UICollectionViewDelegateFlowLayout {
  /**
   Set size of item layout
   */
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 258, height: 34)
  }
}
