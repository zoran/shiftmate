//
//  AddShiftTimesViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC enables a user to define a list of shift times, accessible via Onboarding or Settings screens
 */
final class AddShiftTimesViewController: UIViewController {
  internal static let storyboardId = "\(AddShiftTimesViewController.self)"

  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var addButton: UIButton!
  @IBOutlet weak var nextButton: UIButton!

  // Custom picker
  private var timeRangePickerView: TimeRangePickerView?
  private var isCustomPickerDisplayed = false
  
  // Collection
  private var collectionDataStore: AddShiftTimesCollectionDataStore?
  private var collectionEditModeEnabled = false

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
    self.createPickerView()
    self.configureCollectionView()
    self.configureNavigationItems()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.timeRangePickerView?.hidePickerLayout(viewController: self)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .shiftTimesLocation)
  }
  
  private func configureUi() {
    self.addButton.standardButtonStyle()
    self.nextButton.ctaStyle()
  }
  
  /**
   Init TimeRangePickerView, apply constraints and callbacks
   */
  private func createPickerView() {
    let pickerView = TimeRangePickerView.instantiateNib()
    self.view.addSubview(pickerView)

    pickerView.anchorToBottomOf(viewController: self)
    
    // Init callbacks
    pickerView.doneCallback = { startTime, endTime in
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
      
      self.collectionDataStore?.addItem(item: ShiftTimeRange(startTime: startTime,
                                                             endTime: endTime,
                                                             isEditEnabled: self.collectionEditModeEnabled))
    }

    pickerView.cancelCallback = {
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
    }
    
    pickerView.resetPickerValuesToDefaultState()

    self.timeRangePickerView = pickerView
  }
  
  private func configureCollectionView() {
    // Set transparent background
    self.collectionView.backgroundColor = .clear
    self.collectionView.backgroundView = UIView(frame: .zero)

    // Register cells
    self.collectionView.register(UINib(nibName: AddShiftTimeCell.reuseId, bundle: nil),
                                 forCellWithReuseIdentifier: AddShiftTimeCell.reuseId)
    
    // Register data store
    self.collectionDataStore = AddShiftTimesCollectionDataStore(self)
    self.collectionView.dataSource = collectionDataStore
    self.collectionView.delegate = collectionDataStore
    
    // Set Collection View layout settings
    let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    flowLayout.sectionInset = UIEdgeInsets(top: 8.0, left: 0.0, bottom: 16.0, right: 0.0) // Add padding to top/bottom of list
    flowLayout.minimumLineSpacing = 12.0 // Item Spacing
  }
  
  /**
   Toggle collection edit state
   */
  private func toggleCollectionEditMode() {
    self.collectionEditModeEnabled = !self.collectionEditModeEnabled
    self.collectionDataStore?.toggleEditVisibility(isEnabled: self.collectionEditModeEnabled)
  }
  
  /**
   Display specific navigation items based on navigation controller type
   */
  private func configureNavigationItems() {
    guard let navController = self.navigationController as? SettingsShiftTimesPreferenceNavigationController else { return }
    
    // Add close button if nav controller is a settings-based navigation controller
    let closeButton = UIBarButtonItem(image: UIImage(named: "ic_close_navigation"),
                                      style: .plain,
                                      target: self,
                                      action: #selector(self.closeButtonTapped(_:)))
    self.navigationItem.setLeftBarButton(closeButton, animated: false)
    
    self.nextButton.setTitle("Done", for: .normal)
  }
  
  @objc func closeButtonTapped(_ sender: Any) {
    if let navController = self.navigationController as? SettingsShiftTimesPreferenceNavigationController {
      navController.updateShiftTimesPreferenceCompleted(wasSuccessful: false)
    }
  }
  
  /**
   Complete shift times vc and follow next step based on navigation controller type
   */
  private func triggerForwardNavigation() {
    if let navController = self.navigationController as? OnboardingNavigationController {
      // Display Commute VC
      navController.pushViewController(ViewUtil.commuteTimeViewController, animated: true)
    }
    
    if let navController = self.navigationController as? SettingsShiftTimesPreferenceNavigationController {
      navController.updateShiftTimesPreferenceCompleted(wasSuccessful: true)
    }
  }

  // MARK: - Button Delegates
  
  @IBAction func addButtonTapped(_ sender: Any) {
    self.timeRangePickerView?.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                          completionCallback: { self.isCustomPickerDisplayed = $0 })
  }

  @IBAction func nextButtonTapped(_ sender: Any) {
    guard let dataStore = self.collectionDataStore else { return }

    if !dataStore.hasItems() {
      let dialog = ViewUtil.createBasicPopupDialog(title: "Shift Time required",
                                                   message: "myShift requires at least one shift time to generate a recommended schedule for you", handler: nil)
      
      self.present(dialog, animated: true, completion: nil)
      return
    }
    
    // ZORAN: This would be a great spot to persist the user selected Shift Time's
    
    self.triggerForwardNavigation()
  }
  
  @IBAction func editBarButtonTapped(_ sender: Any) {
    self.toggleCollectionEditMode()
  }
}

