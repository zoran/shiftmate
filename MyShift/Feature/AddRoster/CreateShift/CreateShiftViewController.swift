//
//  CreateShiftViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC handles creating a shift allowing the user to select from, or create, a shift time and shift type
 */
final class CreateShiftViewController: UIViewController {
  internal static let storyboardId = "\(CreateShiftViewController.self)"

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var selectedTimeButton: UIButton!
  @IBOutlet weak var createShiftTypeButton: UIButton!
  @IBOutlet weak var editBarButtonItem: UIBarButtonItem!
  
  @IBOutlet weak var shiftTimeCollectionView: UICollectionView!
  @IBOutlet weak var shiftTypeCollectionView: UICollectionView!

  @IBOutlet weak var selectedTimeView: UIView!
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var bottomView: UIView!

  private var shiftTimeCollectionDataStore: CreateShiftTimeCollectionDataStore?
  private var shiftTypeCollectionDataStore: CreateShiftTypeCollectionDataStore?
  private var shiftTypeCollectionEditModeEnabled = false
  
  // Custom picker
  private var timeRangePickerView: TimeRangePickerView?
  private var isCustomPickerDisplayed = false
  
  // VC setup params
  var contextDate: Date?
  var pageControlPosition: Int?

  private var topShiftStackVisible = true
  
  // Shift configuration instance variables
  private var selectedShiftTime: SelectRosterShiftTime?
  private var selectedShiftType: SelectRosterShiftType?

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
    self.configureShiftTimeCollectionView()
    self.configureShiftTypeCollectionView()
    
    // ZORAN: - This will be a great spot to fetch and load in saved Shift Time/Type's
    self.shiftTimeCollectionDataStore?.setItems(itemList: MockDataUtil.addRosterShiftTimeExample())
    self.shiftTypeCollectionDataStore?.setItems(itemList: MockDataUtil.addRosterShiftTypeExample())
    
    self.createPickerView()
    self.addNavigationButtons()
    
    // Set Date label
    if let contextDate = self.contextDate {
      self.dateLabel.text = "\(contextDate.dayName()) \(contextDate.dayDateSuffix()) \(contextDate.monthYear())"
    }

    // Show Shift Time stack first
    self.showTopShiftStack()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.timeRangePickerView?.hidePickerLayout(viewController: self)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    if let navController = self.navigationController as? AddRosterNavigationController,
      let pageControlPosition = self.pageControlPosition {
      navController.updatePageControl(position: pageControlPosition)
    }
  }
  
  private func configureUi() {
    self.selectedTimeButton.standardButtonStyle()
    self.selectedTimeButton.layer.borderWidth = 0
    self.selectedTimeButton.backgroundColor = ViewUtil.shiftTimeSelectBackgroundSelectedColor
    self.selectedTimeButton.isUserInteractionEnabled = false
    self.selectedTimeButton.setTitle("", for: .normal)
    self.selectedTimeButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .medium)
    
    self.createShiftTypeButton.standardButtonStyle()
    self.createShiftTypeButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .medium)
  }

  private func configureShiftTimeCollectionView() {
    // Set transparent background
    self.shiftTimeCollectionView.backgroundColor = .clear
    self.shiftTimeCollectionView.backgroundView = UIView(frame: .zero)
    self.shiftTimeCollectionView.keyboardDismissMode = .onDrag
    self.shiftTimeCollectionView.delaysContentTouches = false
    
    // Register cells
    self.shiftTimeCollectionView.register(UINib(nibName: SelectShiftTimeCell.reuseId, bundle: nil),
                                          forCellWithReuseIdentifier: SelectShiftTimeCell.reuseId)
    
    // Register data store
    self.shiftTimeCollectionDataStore = CreateShiftTimeCollectionDataStore(self)
    self.shiftTimeCollectionView.dataSource = self.shiftTimeCollectionDataStore
    self.shiftTimeCollectionView.delegate = self.shiftTimeCollectionDataStore
    
    // Set Collection View layout settings
    let flowLayout = self.shiftTimeCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
    flowLayout.sectionInset = UIEdgeInsets(top: 24.0, left: 0.0, bottom: 0.0, right: 0.0) // Add padding to top/bottom of list
    flowLayout.minimumLineSpacing = 12.0 // Item Spacing
  }
  
  private func configureShiftTypeCollectionView() {
    // Set transparent background
    self.shiftTypeCollectionView.backgroundColor = .clear
    self.shiftTypeCollectionView.backgroundView = UIView(frame: .zero)
    self.shiftTypeCollectionView.keyboardDismissMode = .onDrag
    self.shiftTypeCollectionView.delaysContentTouches = false
    
    // Register cells
    self.shiftTypeCollectionView.register(UINib(nibName: SelectShiftTypeCell.reuseId, bundle: nil),
                                          forCellWithReuseIdentifier: SelectShiftTypeCell.reuseId)
    
    // Register data store
    self.shiftTypeCollectionDataStore = CreateShiftTypeCollectionDataStore(self)
    self.shiftTypeCollectionView.dataSource = self.shiftTypeCollectionDataStore
    self.shiftTypeCollectionView.delegate = self.shiftTypeCollectionDataStore
    
    // Set Collection View layout settings
    let flowLayout = self.shiftTypeCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
    flowLayout.sectionInset = UIEdgeInsets(top: 16.0, left: 0.0, bottom: 0.0, right: 0.0) // Add padding to top/bottom of list
    flowLayout.minimumLineSpacing = 12.0 // Item Spacing
  }
  
  private func toggleDisplayShiftTimeTypeStack() {
    self.topShiftStackVisible = !self.topShiftStackVisible
    self.topShiftStackVisible ? self.showTopShiftStack() : self.showBottomShiftStack()
  }
  
  private func showTopShiftStack() {
    self.topView.isHidden = false
    self.bottomView.isHidden = true
    self.selectedTimeView.isHidden = true
    self.selectedTimeButton.isHidden = true
    
    // Hide Edit button
    self.editBarButtonItem.isEnabled = false
    self.editBarButtonItem.tintColor = .clear
  }
  
  private func showBottomShiftStack() {
    self.topView.isHidden = true
    self.bottomView.isHidden = false
    self.selectedTimeView.isHidden = false
    self.selectedTimeButton.isHidden = false
    
    // Show Edit button
    self.editBarButtonItem.isEnabled = true
    self.editBarButtonItem.tintColor = .white
  }
  
  /**
   Display custom Time Picker for custom shift time selection
   */
  func triggerShiftTimeTap(item: SelectRosterShiftTime) {
    switch item.type {
    case .timeRange:  self.selectTimeRange(item: item)
    case .dayOff:     self.selectTimeRange(item: item)
    case .custom:
      self.timeRangePickerView?.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                                           completionCallback: { self.isCustomPickerDisplayed = $0 })
    }
  }
  
  /**
   Apply Time Range selection, update title button label and display Shift Type total
   */
  private func selectTimeRange(item: SelectRosterShiftTime) {
    self.selectedShiftTime = item
    
    // Set label
    if item.type == .timeRange {
      if let startTime = item.startTime,
        let endTime = item.endTime {
        self.selectedTimeButton.setTitle("\(startTime) - \(endTime)", for: .normal)
      }
    } else if item.type == .dayOff {
      self.selectedTimeButton.setTitle("Day Off", for: .normal)
      self.triggerSelectShiftType(item: CreateShiftTypeCollectionDataStore.skipShiftType)
    }
    
    self.toggleDisplayShiftTimeTypeStack()
  }
  
  /**
   Trigger necessary navigation controller action once a shift type is selected
   */
  func triggerSelectShiftType(item: SelectRosterShiftType) {
    self.selectedShiftType = item

    // Save shift type item
  
    if let navController = self.navigationController as? CreateSingleShiftNavigationController {
      navController.addShiftCompleted(success: true)
      return
    }
    
    
    if let navController = self.navigationController as? AddRosterNavigationController {
      navController.moveToNextViewController()
      return
    }
  }
  
  /**
   Init TimeRangePickerView, apply constraints and callbacks
   */
  private func createPickerView() {
    let pickerView = TimeRangePickerView.instantiateNib()
    self.view.addSubview(pickerView)
    
    pickerView.anchorToBottomOf(viewController: self)

    // Init callbacks
    pickerView.doneCallback = { startTime, endTime in
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
      
      self.selectTimeRange(item: SelectRosterShiftTime(startTime: startTime, endTime: endTime, type: .timeRange))
    }
    
    pickerView.cancelCallback = {
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
    }
    
    pickerView.resetPickerValuesToDefaultState()
    
    self.timeRangePickerView = pickerView
  }
  
  private func addNavigationButtons() {
    if self.navigationController is CreateSingleShiftNavigationController {
      let closeButton = UIBarButtonItem(image: UIImage(named: "ic_close_navigation"), style: .plain, target: self, action: #selector(closeSingleAddShift(_:)))

      self.navigationItem.setLeftBarButton(closeButton, animated: false)
    }
  }
  
  @objc func closeSingleAddShift(_ selector : Any?) {
    if let navController = self.navigationController as? CreateSingleShiftNavigationController {
      navController.addShiftCompleted(success: false)
      return
    }
  }

  /**
   Return an object containing the date, and selected shift time/type
   */
  func getCreatedShiftDetails() -> CreatedShiftDetails {
    return CreatedShiftDetails(date: self.contextDate,
                        time: self.selectedShiftTime,
                        type: self.selectedShiftType)
  }
  
  /**
   Display an alert controller to add a shift type
   */
  private func showCreateShiftTypeDialog() {
    let alertController = UIAlertController(title: "Shift Type", message: "", preferredStyle: .alert)
    
    alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    alertController.addAction(UIAlertAction(title: "Add", style: .default, handler: { alert -> Void in
      let textField = alertController.textFields![0] as UITextField
      
      // Save input text as a Shift Type, and add it to the collection
      if let inputText = textField.text {
        if inputText.count == 0 { return }
        
        self.shiftTypeCollectionDataStore?.addItem(item: SelectRosterShiftType(typeName: inputText,
                                                                               isRemovable: true,
                                                                               isEditEnabled: self.shiftTypeCollectionEditModeEnabled))
      }
    }))
    
    alertController.addTextField(configurationHandler: { textField in
      textField.placeholder = "Enter a new shift type"
    })
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: - Button Delegates
    
  @IBAction func editButtonTapped(_ sender: Any) {
    self.shiftTypeCollectionEditModeEnabled = !self.shiftTypeCollectionEditModeEnabled
    
    self.shiftTypeCollectionDataStore?.toggleEditVisibility(isEnabled: self.shiftTypeCollectionEditModeEnabled)
  }

  @IBAction func createShiftTypeButtonTapped(_ sender: Any) {
    self.showCreateShiftTypeDialog()
  }
}
