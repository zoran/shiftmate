//
//  CreateShiftTypeCollectionDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 22/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of item's, handles cell styling and list modification/updates
 */
final class CreateShiftTypeCollectionDataStore: NSObject {
  fileprivate var items = [SelectRosterShiftType]()
  private var parentController: CreateShiftViewController?
  
  // "Skip" Shift Type
  static let skipShiftType = SelectRosterShiftType(typeName: nil, isRemovable: false, isEditEnabled: false)
  
  init(_ controller: CreateShiftViewController) {
    super.init()
    self.parentController = controller
  }
  
  func setItems(itemList: [SelectRosterShiftType]) {
    self.items.removeAll()
    
    self.items.append(contentsOf: itemList)
    self.parentController?.shiftTypeCollectionView.reloadData()
  }
  
  /**
   Add item and refresh collection
   */
  func addItem(item: SelectRosterShiftType) {
    if self.items.count == 0 {
      self.items.append(item)
    } else {
      self.items.insert(item, at: self.items.count - 1)
    }

    self.parentController?.shiftTypeCollectionView.reloadData()
  }

  /**
   Remove item based on index position
   */
  func removeItem(index: Int) {
    self.items.remove(at: index)
    self.parentController?.shiftTypeCollectionView.reloadData()
  }
  
  func hasItems() -> Bool {
    return !self.items.isEmpty
  }
  
  /**
   Update edit state variable for collection list model's, and refresh collection view
   */
  func toggleEditVisibility(isEnabled: Bool) {
    for (index, _) in self.items.enumerated() {
      // Prevent edit state toggle if item
      if self.items[index].isRemovable {
        self.items[index].toggleEditState()
      }
    }
    
    self.parentController?.shiftTypeCollectionView.reloadData()
  }
  
  func triggerSelectShiftType(item: SelectRosterShiftType) {
    self.parentController?.triggerSelectShiftType(item: item)
  }
}

// MARK: - UICollectionViewDataSource
extension CreateShiftTypeCollectionDataStore: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelectShiftTypeCell.reuseId, for: indexPath) as! SelectShiftTypeCell
    
    cell.tag = indexPath.item // Set cell tag as node position
    cell.configureCell(dataStore: self)
    cell.populateUi(item: items[indexPath.item])
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.items.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

// MARK: - UICollectionViewDelegate
extension CreateShiftTypeCollectionDataStore: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {    
    collectionView.deselectItem(at: indexPath, animated: false) // Deselect item
  }
  
  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as? SelectShiftTypeCell
    
    cell?.shiftTypeLabel.backgroundColor = ViewUtil.shiftTimeSelectBackgroundSelectedColor // Show background selected state
    cell?.shiftTypeLabel.layer.borderWidth = 0 // Hide border
  }
  
  func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as? SelectShiftTypeCell
    cell?.shiftTypeLabel.backgroundColor = .clear // Clear background selected state
    cell?.shiftTypeLabel.layer.borderWidth = 1 // Show border
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CreateShiftTypeCollectionDataStore: UICollectionViewDelegateFlowLayout {
  /**
   Set size of item layout
   */
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 258, height: 34)
  }
}
