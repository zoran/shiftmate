//
//  SelectShiftTypeCell.swift
//  MyShift
//
//  Created by Michael Tigas on 22/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom collection cell to denote a Shift Type cell
 */
final class SelectShiftTypeCell: UICollectionViewCell {
  internal static let reuseId = "\(SelectShiftTypeCell.self)"

  @IBOutlet weak var shiftTypeLabel: UILabel!
  @IBOutlet weak var deleteButton: UIButton!
  
  private var dataStore: CreateShiftTypeCollectionDataStore?
  private var shiftType: SelectRosterShiftType?
  
  /**
   Configure cell values & UI
   */
  internal func configureCell(dataStore: CreateShiftTypeCollectionDataStore) {
    self.dataStore = dataStore
    
    self.configureGestures()
    self.configureUi()
  }
  
  private func configureUi() {
    self.shiftTypeLabel.layer.masksToBounds = true
    self.shiftTypeLabel.layer.cornerRadius = 18
    self.shiftTypeLabel.layer.borderWidth = 1
    self.shiftTypeLabel.layer.borderColor = ViewUtil.shiftTimeTypeBorderColor.cgColor
    self.shiftTypeLabel.tintColor = .white
  }

  /**
   Populate cell values
   */
  internal func populateUi(item: SelectRosterShiftType) {
    self.resetUi()
    
    self.shiftType = item
    
    // Set Name label
    if item.isRemovable {
      if let name = item.typeName {
        self.shiftTypeLabel.text = name
      }
    } else {
      self.shiftTypeLabel.text = "Skip"
    }
    
    self.deleteButton.isHidden = !item.isEditEnabled
  }

  func toggleDeleteButtonVisibility(isVisible: Bool) {
    self.deleteButton.isHidden = !isVisible
  }

  private func configureGestures() {
    // General Background View Tap
    let backgroundTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundViewTapped(_:)))
    self.contentView.addGestureRecognizer(backgroundTapGesture)
  }
  
  /**
   Send open activity action via DataStore
   */
  @objc func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
    if let shiftType = self.shiftType { self.dataStore?.triggerSelectShiftType(item: shiftType) }
  }

  internal func cellFinishedDisplaying() {
    // Remove tap gestures
    if let gestureRecognizers = self.contentView.gestureRecognizers {
      for gesture in gestureRecognizers {
        self.contentView.removeGestureRecognizer(gesture)
      }
    }
  }
  
  private func resetUi() {
    self.shiftType = nil
    self.shiftTypeLabel.text = ""
  }
  
  // MARK: - Button delegates
  
  @IBAction func deleteButtonTapped(_ sender: Any) {
    self.dataStore?.removeItem(index: self.tag)
  }
}
