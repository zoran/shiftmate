//
//  SelectShiftTimeCell.swift
//  MyShift
//
//  Created by Michael Tigas on 22/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 A custom collection cell to denote a Shift Time cell
 */
final class SelectShiftTimeCell: UICollectionViewCell {
  internal static let reuseId = "\(SelectShiftTimeCell.self)"

  @IBOutlet weak var timeLabel: UILabel!

  private var dataStore: CreateShiftTimeCollectionDataStore?
  private var shiftTime: SelectRosterShiftTime?
  
  /**
   Configure cell values & UI
   */
  internal func configureCell(dataStore: CreateShiftTimeCollectionDataStore) {
    self.dataStore = dataStore
    
    self.configureUi()
  }
  
  private func configureUi() {
    self.timeLabel.layer.masksToBounds = true
    self.timeLabel.layer.cornerRadius = 18
    self.timeLabel.layer.borderWidth = 1
    self.timeLabel.layer.borderColor = ViewUtil.shiftTimeTypeBorderColor.cgColor
    self.timeLabel.tintColor = .white
  }
  
  /**
   Populate cell values
   */
  internal func populateUi(item: SelectRosterShiftTime) {
    self.resetUi()
    
    self.shiftTime = item
    
    let labelText: String
    switch item.type {
    case .timeRange: // Display time range label, e.g "23:00 - 07:00"
      if let startTime = item.startTime,
        let endTime = item.endTime {
        labelText = "\(startTime) - \(endTime)"
      } else {
        labelText = ""
      }
    case .dayOff:
      labelText = "Day Off"
    case .custom:
      labelText = "Custom"
    }
    
    self.timeLabel.text = labelText
  }
  
  private func resetUi() {
    self.shiftTime = nil
    self.timeLabel.text = ""
  }
}
