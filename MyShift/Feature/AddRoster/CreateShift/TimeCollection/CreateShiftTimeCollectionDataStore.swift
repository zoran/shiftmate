//
//  CreateShiftTimeCollectionDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 22/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This data store contains a list of item's, handles cell styling and list modification/updates
 */
final class CreateShiftTimeCollectionDataStore: NSObject {
  fileprivate var items = [SelectRosterShiftTime]()
  private var parentController: CreateShiftViewController?
  
  init(_ controller: CreateShiftViewController) {
    super.init()
    self.parentController = controller
  }
  
  func setItems(itemList: [SelectRosterShiftTime]) {
    self.items.removeAll()
    
    self.items.append(contentsOf: itemList)
    self.parentController?.shiftTimeCollectionView?.reloadData()
  }
}

// MARK: - UICollectionViewDataSource
extension CreateShiftTimeCollectionDataStore: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelectShiftTimeCell.reuseId, for: indexPath) as! SelectShiftTimeCell
    
    cell.configureCell(dataStore: self)
    cell.populateUi(item: items[indexPath.item])
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.items.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

// MARK: - UICollectionViewDelegate
extension CreateShiftTimeCollectionDataStore: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let selectedItem = items[indexPath.item]
    
    self.parentController?.triggerShiftTimeTap(item: selectedItem)
    
    collectionView.deselectItem(at: indexPath, animated: false) // Deselect item
  }
  
  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as? SelectShiftTimeCell
    cell?.timeLabel.backgroundColor = ViewUtil.shiftTimeSelectBackgroundSelectedColor // Show background selected state
    cell?.timeLabel.layer.borderWidth = 0 // Hide border
  }
  
  func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as? SelectShiftTimeCell
    cell?.timeLabel.backgroundColor = .clear // Clear background selected state
    cell?.timeLabel.layer.borderWidth = 1 // Show border
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CreateShiftTimeCollectionDataStore: UICollectionViewDelegateFlowLayout {
  /**
   Set size of item layout
   */
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 258, height: 34)
  }
}
