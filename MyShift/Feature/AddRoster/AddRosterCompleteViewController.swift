//
//  AddRosterCompleteViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC displays a simple completed message, with total shift added count
 */
final class AddRosterCompleteViewController: UIViewController {
  internal static let storyboardId = "\(AddRosterCompleteViewController.self)"

  @IBOutlet weak var rosterAddedDescription: UILabel!
  @IBOutlet weak var okButton: UIButton!
  
  // VC setup params
  var totalShiftCreatedCount: Int?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.okButton.ctaStyle()
    
    if let count = totalShiftCreatedCount {
      self.rosterAddedDescription.text = "Your \(count) roster has been successfully imported."
    }
    
    (self.navigationController as? AddRosterNavigationController)?.hidePageControl()
  }
  
  // MARK: - Button Delegates

  @IBAction func okButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
}
