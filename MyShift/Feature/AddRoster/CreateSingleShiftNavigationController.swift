//
//  CreateSingleShiftNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 22/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This navigation controller displays only a single child VC to create a shift for a
 specific date
 */
final class CreateSingleShiftNavigationController: UINavigationController {
  internal static let storyboardId = "\(CreateSingleShiftNavigationController.self)"
  
  // VC setup parameters
  var successCallback, cancelCallback: (() -> ())?
  var contextDate: Date?

  override func viewDidLoad() {
    super.viewDidLoad()
    setNeedsStatusBarAppearanceUpdate()

    // Init a Create Shift VC
    let singleDayVc = ViewUtil.createShiftViewController
    singleDayVc.contextDate = contextDate

    setViewControllers([singleDayVc], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  /**
   Dismiss Navigation Controller, and trigger callback based on whether add shift
   was successful
   */
  func addShiftCompleted(success: Bool) {
    self.dismiss(animated: true, completion: nil)

    success ? self.successCallback?() : self.cancelCallback?()
  }
}
