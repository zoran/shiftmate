//
//  AddRosterNavigationController.swift
//  MyShift
//
//  Created by Michael Tigas on 24/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This navigation controller initially displays a "Select Dates" vc, then after a calendar date range has been selected
 it will display subsequent Create Shift vc's
 */
final class AddRosterNavigationController: UINavigationController {
  internal static let storyboardId = "\(AddRosterNavigationController.self)"

  // Navigation stack preferences
  private var createShiftNavStack: [CreateShiftViewController] = []
  private var totalViewControllerCount: Int = 0
  private var currentViewControllerPosition: Int = 0
  
  // Page Control progress
  private var addRosterPageControl: UIPageControl?
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.setNeedsStatusBarAppearanceUpdate()
    self.delegate = self
    self.useCustomBackIndicator()

    // Init with Select Dates VC
    self.setViewControllers([ViewUtil.selectDatesViewController], animated: true)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  /**
   Create a list of view controllers based on selected date range
   */
  func generateAndShowVCsForDateList(selectedDates: [Date]) {
    self.resetVcGroupState()

    for (index, date) in selectedDates.enumerated() { // Index used to maintain Page Control positioning
      self.createShiftNavStack.append(ViewUtil.createShiftViewController(withDate: date, pageControlPosition: index))
    }

    self.totalViewControllerCount = self.createShiftNavStack.count // Retain total VC count

    self.attachAndShowPageControl()
    
    // Display initial Create Shift VC
    self.pushViewController(self.createShiftNavStack[self.currentViewControllerPosition], animated: true)
  }

  /**
   Display the next view controller in the collection
   */
  func moveToNextViewController() {
    self.currentViewControllerPosition = self.currentViewControllerPosition + 1

    // If there are no further VC's, save shift details from previous vc's
    if self.currentViewControllerPosition == self.totalViewControllerCount {
      self.saveAllShiftDetails()
      return
    }

    self.pushViewController(self.createShiftNavStack[self.currentViewControllerPosition], animated: true)
  }
  
  /**
   Retrieve shift details from all VC's and display the roster complete VC
   */
  func saveAllShiftDetails() {
    var createdShiftList: [CreatedShiftDetails] = []
    
    for vc in self.createShiftNavStack {
      createdShiftList.append(vc.getCreatedShiftDetails())
    }
    
    // ZORAN: This would be a great place to persist the defined shift details
    
    self.setViewControllers([ViewUtil.addRosterCompleteViewController(withShiftTotal: createdShiftList.count)], animated: true)
  }
  
  /**
   Clear state of previous created vc's - important for when the user returns back to the initial Date Select vc
   */
  private func resetVcGroupState() {
    self.createShiftNavStack.removeAll()
    self.totalViewControllerCount = 0
    self.currentViewControllerPosition = 0
  }
  
  /**
   Display a Page Control at the top of the Navigation Controller
   */
  private func attachAndShowPageControl() {
    if self.addRosterPageControl == nil {
      let pageControl = UIPageControl(frame: self.navigationBar.bounds)
      self.navigationBar.addSubview(pageControl)
      self.addRosterPageControl = pageControl
    }

    self.addRosterPageControl?.isUserInteractionEnabled = false // Prevent absorbing user taps
    self.addRosterPageControl?.isHidden = false
    
    // Start, Your WR, Select Roster, Shift Times/Shift Location, Commute, Caffeine, Notifications
    self.addRosterPageControl?.numberOfPages = self.totalViewControllerCount
    self.addRosterPageControl?.currentPage = 0
  }
  
  /**
   Update page control position
   */
  func updatePageControl(position: Int) {
    if position < 0 { return }
    
    guard let pageControl = self.addRosterPageControl else { return }
    
    if position > pageControl.numberOfPages - 1 { return }
    
    pageControl.currentPage = position
  }
  
  func hidePageControl() {
    self.addRosterPageControl?.isHidden = true
  }
}

// MARK: - UINavigationControllerDelegate
extension AddRosterNavigationController: UINavigationControllerDelegate {
  func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    // Do not display a VC title
    // This will also prevent previous VC title being shown in Back button
    viewController.title = " "
  }
}
