//
//  SelectDatesCalendarDataStore.swift
//  MyShift
//
//  Created by Michael Tigas on 24/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation
import FSCalendar

/**
 This data store handles necessary Calendar cell configuration for the Select Dates VC
 */
final class SelectDatesCalendarDataStore: NSObject {
  private var parentController: SelectDatesViewController?
  
  init(_ controller: SelectDatesViewController) {
    super.init()
    self.parentController = controller
  }
  
  /**
   Reconfigure all visible date cells
   */
  private func configureAllVisibleCells(_ calendar: FSCalendar) {
    calendar.visibleCells()
      .forEach({ cell in
        if let date = calendar.date(for: cell) {
          let position = calendar.monthPosition(for: cell)
          
          self.configureDateCell(cell, for: date, at: position)
        }
      })
  }
  
  /**
   Configure the date cell's selection type
   */
  private func configureDateCell(_ cell: FSCalendarCell,
                                 for date: Date,
                                 at position: FSCalendarMonthPosition) {
    guard let dateCell = cell as? CalendarDateViewCell else { return }
    
    switch position {
    case .current:
      let selectedDates = cell.calendar.selectedDates
      var selectionType = CalendarDateViewCell.SelectionType.none
      
      // Determine selection type based on date ranges
      if !selectedDates.contains(date) {
        selectionType = .none
        dateCell.selectionLayer.isHidden = true
        return
      }
      
      if let previousDate = cell.calendar.gregorian.date(byAdding: .day, value: -1, to: date),
        let nextDate = cell.calendar.gregorian.date(byAdding: .day, value: 1, to: date) {
        
        if selectedDates.contains(date) {
          if selectedDates.contains(previousDate) && selectedDates.contains(nextDate) {
            selectionType = .middle
          } else if selectedDates.contains(previousDate) && selectedDates.contains(date) {
            selectionType = .rightBorder
          } else if selectedDates.contains(nextDate) {
            selectionType = .leftBorder
          } else {
            selectionType = .single
          }
        }
      }
      
      dateCell.selectionLayer.isHidden = false
      dateCell.selectionType = selectionType
    default:
      dateCell.selectionLayer.isHidden = true
    }
  }
}

// MARK: - FSCalendarDataSource
extension SelectDatesCalendarDataStore: FSCalendarDataSource {
  func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
    let cell = calendar.dequeueReusableCell(withIdentifier: CalendarDateViewCell.reuseId, for: date, at: position)
    
    // If calendar cell is of type CalendarDateViewCell, apply custom formatting
    if let customCell = cell as? CalendarDateViewCell {
      customCell.applyTitleFormatting()
    }
    
    return cell
  }
  
  func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
    self.configureDateCell(cell, for: date, at: monthPosition) // Configure Date Cell when displayed
  }
  
  func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
    // If user selects the initial calendar date, also select the following fortnight
    if calendar.selectedDates.count == 1 {
      for value in 1...13 {
        let nextDate = calendar.gregorian.date(byAdding: .day, value: value, to: date)
        calendar.select(nextDate)
      }
    }
    
    self.parentController?.dateRangeUpdated(dateList: calendar.selectedDates) // Send updated date range to parent controller
    
    self.configureAllVisibleCells(calendar) // Reconfigure all date cells
  }
  
  func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
    self.parentController?.dateRangeUpdated(dateList: calendar.selectedDates) // Send updated date range to parent controller
    self.configureAllVisibleCells(calendar) // Reconfigure all date cells
  }
  
  func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
    return monthPosition == .current
  }
  
  func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
    return monthPosition == .current
  }
}

// MARK: - FSCalendarDelegate
extension SelectDatesCalendarDataStore: FSCalendarDelegate {
  func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
    self.parentController?.fsCalendar?.frame = CGRect(origin: calendar.frame.origin, size: bounds.size)
  }
}
