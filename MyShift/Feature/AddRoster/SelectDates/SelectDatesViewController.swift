//
//  SelectDatesViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit
import FSCalendar

/**
 This VC displays a calendar which users can select various dates to generate shifts for
 */
final class SelectDatesViewController: UIViewController {
  internal static let storyboardId = "\(SelectDatesViewController.self)"

  @IBOutlet weak var dateRangeLabel: UILabel!
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var calendarContainerView: UIView!
  @IBOutlet weak var calendarCurrentMonthLabel: UILabel!
  
  // Calendar
  var fsCalendar: FSCalendar?
  private var calendarDataStore: SelectDatesCalendarDataStore?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.nextButton.ctaStyle()
    self.attachCalendarToView()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    (self.navigationController as? AddRosterNavigationController)?.hidePageControl()
  }

  private func attachCalendarToView() {
    // Init calendar to match container view bounds
    let calendar = FSCalendar(frame: self.calendarContainerView.bounds)
    
    self.calendarDataStore = SelectDatesCalendarDataStore(self)
    calendar.dataSource = self.calendarDataStore
    calendar.delegate = self.calendarDataStore
    
    calendar.applyCustomStyle()
    
    // Register StyledCalendarDayViewCell for use in creating new calendar cells
    calendar.register(CalendarDateViewCell.self, forCellReuseIdentifier: CalendarDateViewCell.reuseId)
    
    // Add calendar to container
    self.calendarContainerView.addSubview(calendar)
    
    // Constrain calendar to its specific container view
    calendar.centerXAnchor.constraint(equalTo: self.calendarContainerView.centerXAnchor).isActive = true
    calendar.centerYAnchor.constraint(equalTo: self.calendarContainerView.centerYAnchor).isActive = true
    calendar.leadingAnchor.constraint(equalTo: self.calendarContainerView.leadingAnchor).isActive = true
    calendar.trailingAnchor.constraint(equalTo: self.calendarContainerView.trailingAnchor).isActive = true
    calendar.topAnchor.constraint(equalTo: self.calendarContainerView.topAnchor).isActive = true
    calendar.bottomAnchor.constraint(equalTo: self.calendarContainerView.bottomAnchor).isActive = true
    calendar.translatesAutoresizingMaskIntoConstraints = false
    
    self.fsCalendar = calendar
    
    // Set current month label
    self.updateCalendarHeaderLabel()
  }
  
  /**
   Update month displayed in calendar header
   */
  private func updateCalendarHeaderLabel() {
    guard let calendar = self.fsCalendar else { return }
    self.calendarCurrentMonthLabel.text = calendar.currentPage.monthYear().uppercased()
  }
  
  /**
   Update the date range label based on specified dates
   */
  func dateRangeUpdated(dateList: [Date]) {
    if dateList.count == 0 {
      self.dateRangeLabel.text = ""
      return
    }
    
    if dateList.count == 1 {
      guard let first = dateList.first else { return }
      self.dateRangeLabel.text = "\(first.dayDate()) \(first.monthYear())"
      return
    }
    
    if dateList.count > 1 {
      let sortedDateList = dateList.orderAscending()

      // Create ranged date string
      if let first = sortedDateList.first,
        let second = sortedDateList.last {
        
        var dateString = ""
        if first.month() == second.month() {
          dateString = "\(first.dayDate()) - \(second.dayDate()) \(first.monthYear())"
        } else {
          dateString = "\(first.dayDate()) \(first.month()) - \(second.dayDate()) \(second.monthYear())"
        }
        
        self.dateRangeLabel.text = dateString
      }

      return
    }
  }
  
  /**
   Move back or forward a month
   */
  func updateCalendarMonth(increment: Bool) {
    guard let calendarPage = self.fsCalendar?.currentPage else { return }

    if let newPage = self.fsCalendar?.gregorian.date(byAdding: .month, value: increment ? 1 : -1, to: calendarPage) {
      self.fsCalendar?.currentPage = newPage
    }
  }
  
  /**
   Validate whether the user has selected at least one date before proceeding
   */
  private func validateCalendarSelection() {
    guard let navController = self.navigationController as? AddRosterNavigationController,
      let calendar = self.fsCalendar else { return }
    
    if calendar.selectedDates.isEmpty {
      let dialog = ViewUtil.createBasicPopupDialog(title: "No Dates Selected",
                                                   message: "Please select at least one calendar day, to proceed with creating your roster schedule",
                                                   handler: nil)
      
      self.present(dialog, animated: true, completion: nil)
      return
    }
    
    navController.generateAndShowVCsForDateList(selectedDates: calendar.selectedDates.orderAscending())
  }
  
  // MARK: - Button Delegates
  
  @IBAction func calendarForwardButtonTapped(_ sender: Any) {
    self.updateCalendarMonth(increment: true)
    self.updateCalendarHeaderLabel()
  }
  
  @IBAction func calendarBackButtonTapped(_ sender: Any) {
    self.updateCalendarMonth(increment: false)
    self.updateCalendarHeaderLabel()
  }

  @IBAction func closeBarButtonTapped(_ sender: Any) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }

  @IBAction func nextButtonTapped(_ sender: Any) {
    self.validateCalendarSelection()
  }
}
