//
//  CommuteTimeViewController.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This VC enables a user to select a commute time, accessible via Onboarding or Settings screens
 */
final class CommuteTimeViewController: UIViewController {
  internal static let storyboardId = "\(CommuteTimeViewController.self)"

  @IBOutlet weak var addButton: UIButton!
  @IBOutlet weak var nextButton: UIButton!
  
  // Custom picker
  private var timeDurationPickerView: TimeDurationPickerView?
  private var isCustomPickerDisplayed = false
  
  private var selectedTimeDuration: TimeInterval?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configureUi()
    self.createPickerView()
    self.configureNavigationItems()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.timeDurationPickerView?.hidePickerLayout(viewController: self)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    (self.navigationController as? OnboardingNavigationController)?.updatePageControl(page: .commute)
  }
  
  private func configureUi() {
    self.addButton.standardButtonStyle()
    self.nextButton.ctaStyle()
  }
  
  /**
   Init TimeDurationPickerView, apply constraints and callbacks
   */
  private func createPickerView() {
    let pickerView = TimeDurationPickerView.instantiateNib()
    self.view.addSubview(pickerView)
    
    pickerView.anchorToBottomOf(viewController: self)
    
    // Init callbacks
    pickerView.doneCallback = { durationSelected in
      self.selectedTimeDuration = durationSelected // Store selected duration

      if let formattedTime = self.selectedTimeDuration?.format() {
        self.addButton.setTitle("\(formattedTime)", for: .normal)
      }

      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
    }
    
    pickerView.cancelCallback = {
      pickerView.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                            completionCallback: { self.isCustomPickerDisplayed = $0 })
      pickerView.resetPickerValuesToDefaultState()
    }
    
    pickerView.resetPickerValuesToDefaultState()
    
    self.timeDurationPickerView = pickerView
  }
  
  /**
   Display specific navigation items based on navigation controller type
   */
  private func configureNavigationItems() {
    guard let navController = self.navigationController as? SettingsCommutePreferenceNavigationController else { return }
    
    // Add close button if nav controller is a settings-based navigation controller
    let closeButton = UIBarButtonItem(image: UIImage(named: "ic_close_navigation"),
                                      style: .plain,
                                      target: self,
                                      action: #selector(self.closeButtonTapped(_:)))
    self.navigationItem.setLeftBarButton(closeButton, animated: false)
    
    self.nextButton.setTitle("Done", for: .normal)
  }
  
  @objc func closeButtonTapped(_ sender: Any) {
    if let navController = self.navigationController as? SettingsCommutePreferenceNavigationController {
      navController.updateCommutePreferenceCompleted(wasSuccessful: false)
    }
  }
  
  /**
   Complete commute time vc and follow next step based on navigation controller type
   */
  private func triggerForwardNavigation() {
    if let navController = self.navigationController as? OnboardingNavigationController {
      // Display Caffeine VC
       navController.pushViewController(ViewUtil.caffeinePreferenceViewController, animated: true)
    }
    
    if let navController = self.navigationController as? SettingsCommutePreferenceNavigationController {
      navController.updateCommutePreferenceCompleted(wasSuccessful: true)
    }
  }

  // MARK: - Button Delegates

  @IBAction func addButtonTapped(_ sender: Any) {
    self.timeDurationPickerView?.togglePickerViewVisibility(self, isPickerVisible: self.isCustomPickerDisplayed,
                                                      completionCallback: { self.isCustomPickerDisplayed = $0 })
    self.timeDurationPickerView?.resetPickerValuesToDefaultState()
  }

  @IBAction func nextButtonTapped(_ sender: Any) {
    guard self.selectedTimeDuration != nil else {
      let dialog = ViewUtil.createBasicPopupDialog(title: "Commute Time required",
                                                   message: "myShift requires a commute time to improve the accuracy of your recommended schedule", handler: nil)
      
      self.present(dialog, animated: true, completion: nil)
      return
    }
    
    // ZORAN: This would be a great spot to persist the user selected Commute Time

    self.triggerForwardNavigation()
  }
}
