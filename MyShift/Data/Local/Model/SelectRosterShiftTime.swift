//
//  SelectRosterShiftTime.swift
//  MyShift
//
//  Created by Michael Tigas on 22/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation

/**
 This object represents a created Shift Time, when a user defines their general shift times
 */
struct SelectRosterShiftTime {
  let startTime, endTime: String?
  let type: ShiftTimeType
  
  /**
   Type of Shift Time
   */
  enum ShiftTimeType {
    case timeRange, dayOff, custom
  }
}
