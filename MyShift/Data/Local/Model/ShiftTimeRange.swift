//
//  ShiftTimeRange.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation

/**
 This object represents a model for the Add Shift Time screen list
 */
struct ShiftTimeRange {
  let startTime, endTime: String
  var isEditEnabled: Bool
  
  mutating func toggleEditState() {
    self.isEditEnabled = !self.isEditEnabled
  }
}
