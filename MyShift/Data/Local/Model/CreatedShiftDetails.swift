//
//  CreatedShiftDetails.swift
//  MyShift
//
//  Created by Michael Tigas on 25/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation

/**
 This object contains a structured proposed Shift, when a user creates a roster or single shift
 */
struct CreatedShiftDetails {
  let date: Date?
  let time: SelectRosterShiftTime?
  let type: SelectRosterShiftType?
}
