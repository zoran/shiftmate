//
//  CaffeinePreference.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation

/**
 This object represents a type of Caffeine Preference when a user defines their caffeine preferences
 */
struct CaffeinePreference {
  let type: CaffeineType
  var isEditEnabled: Bool
  
  mutating func toggleEditState() {
    self.isEditEnabled = !self.isEditEnabled
  }
  
  /**
   Various types of caffeine
   */
  enum CaffeineType {
    case none, tea, coffee,
    energyDrink, cocoaDrink, colaDrink,
    mountainDew, other
    
    var name: String {
      switch self {
      case .none:         return "None"
      case .tea:          return "Tea"
      case .coffee:       return "Coffee"
      case .energyDrink:  return "Energy Drink"
      case .cocoaDrink:   return "Cocoa Drink (Iced or Hot Chocolate)"
      case .colaDrink:    return "Coke/Pepsi"
      case .mountainDew:  return "Mountain Dew"
      case .other:        return "Other"
      }
    }
  }
}
