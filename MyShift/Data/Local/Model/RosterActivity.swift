//
//  RosterActivity.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

/**
 This object represents a scheduled roster activity on the Scheduled List and Roster list screen
 */
struct RosterActivity {
  // Database populated variables
  let activityType: ActivityType
  let startDate: Date
  let endDate: Date?
  let workShiftType: String?
  var workNotes: String?
  var workOvertimeHours: Int?
  var caffeineDrinkType: String?

  // Instance variables
  var isFirstScheduledForDay, isCurrentLive: Bool
  
  /**
   Type of roster activity
   */
  enum ActivityType {
    case work, dayOff, sleep, lightExposure, avoidLight, caffeine, other
    
    var name: String {
      switch self {
      case .work:           return "Work"
      case .dayOff:         return "Day Off"
      case .sleep:          return "Sleep"
      case .lightExposure:  return "Blue/White Light Exposure"
      case .avoidLight:     return "Avoid Blue/White Light"
      case .caffeine:       return "Caffeine"
      case .other:          return "Other"
      }
    }
    
    var color: UIColor {
      switch self {
      case .work:           return UIColor(red: 63/255.0, green: 90/255.0, blue: 220/255.0, alpha: 255/255.0)
      case .dayOff:         return UIColor(red: 154/255.0, green: 195/255.0, blue: 0/255.0, alpha: 255/255.0)
      case .sleep:          return UIColor(red: 156/255.0, green: 126/255.0, blue: 212/255.0, alpha: 255/255.0)
      case .lightExposure:  return UIColor(red: 62/255.0, green: 138/255.0, blue: 229/255.0, alpha: 255/255.0)
      case .avoidLight:     return UIColor(red: 228/255.0, green: 163/255.0, blue: 0/255.0, alpha: 255/255.0)
      case .caffeine:       return UIColor(red: 208/255.0, green: 99/255.0, blue: 33/255.0, alpha: 255/255.0)
      case .other:          return UIColor(red: 0/255.0, green: 152/255.0, blue: 0/255.0, alpha: 255/255.0)
      }
    }
    
    var icon: UIImage? {
      switch self {
      case .work:           return UIImage(named: "ic_work")
      case .dayOff:         return UIImage(named: "ic_dayoff")
      case .sleep:          return UIImage(named: "ic_sleep")
      case .lightExposure:  return UIImage(named: "ic_lightson")
      case .avoidLight:     return UIImage(named: "ic_lightsoff")
      case .caffeine:       return UIImage(named: "ic_caffeine")
      case .other:          return UIImage(named: "ic_other")
      }
    }
  }
  
  /**
   Generate a text string representing the state of an exported work shift or day off
   // Tue 21 August
   // Watch House
   // 10:00 - 11:00
   */
  func exportActivityText() -> String {
    let dateLine = "\(startDate.dayName()) \(startDate.dayDate()) \(startDate.month())" // Tue 21 August
    
    let shiftDetailsLine: String
    switch activityType {
    case .dayOff: shiftDetailsLine = activityType.name // Day Off
    case .work:
      var shiftType = ""
      if let workShiftType = workShiftType { shiftType = workShiftType }
      
      var shiftDateLine = ""
      if let endDate = endDate {
        shiftDateLine = "\(startDate.timeTwentyFourHr()) - \(endDate.timeTwentyFourHr())"
      } else {
        shiftDateLine = startDate.timeTwentyFourHr()
      }

      shiftDetailsLine = "\(shiftType)\n\(shiftDateLine)"  // Watch House \n 10:00 - 11:00
    default: shiftDetailsLine = ""
    }
    
    return "\(dateLine)\n\(shiftDetailsLine)\n\n"
  }
}
