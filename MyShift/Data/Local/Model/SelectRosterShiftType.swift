//
//  SelectRosterShiftType.swift
//  MyShift
//
//  Created by Michael Tigas on 22/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation

/**
 This object represents a created Shift Type, when a user defines their general shift types
 */
struct SelectRosterShiftType {
  let typeName: String?
  let isRemovable: Bool

  var isEditEnabled: Bool
  
  mutating func toggleEditState() {
    self.isEditEnabled = !self.isEditEnabled
  }
}
