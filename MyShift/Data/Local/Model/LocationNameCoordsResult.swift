//
//  LocationNameCoordsResult.swift
//  MyShift
//
//  Created by Michael Tigas on 18/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation
import CoreLocation

/**
 This object represents a resolved address name and location cooordinates. Used when resolving a location search
 to geo-coords
 */
struct LocationNameCoordsResult {
  let address: String
  let latitude, longitude: CLLocationDegrees
}
