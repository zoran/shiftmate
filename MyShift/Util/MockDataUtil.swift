//
//  MockDataUtil.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation

/**
 This util class contains any mock data to populate the app with
 */
struct MockDataUtil {
  private init() {
  }
  
  static func activityListExample() -> [RosterActivity] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm ZZZ" // This will parse: "21/08/2018 10:00 +1000"
    
    let activityOneWork = RosterActivity(activityType: .work,
                                         startDate: dateFormatter.date(from: "21/08/2018 10:00 +1000")!,
                                         endDate: dateFormatter.date(from: "21/08/2018 11:00 +1000")!,
                                         workShiftType: "Watch House", workNotes: "", workOvertimeHours: 2,
                                         caffeineDrinkType: nil,
                                         isFirstScheduledForDay: true, isCurrentLive: true)
    
    let activityTwoCaffeine = RosterActivity(activityType: .caffeine,
                                             startDate: dateFormatter.date(from: "21/08/2018 12:00 +1000")!,
                                             endDate: nil,
                                             workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                             caffeineDrinkType: "coffee",
                                             isFirstScheduledForDay: false, isCurrentLive: true)
    
    
    let activityThreeOther = RosterActivity(activityType: .avoidLight,
                                            startDate: dateFormatter.date(from: "22/08/2018 07:15 +1000")!,
                                            endDate: dateFormatter.date(from: "22/08/2018 07:25 +1000")!,
                                            workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                            caffeineDrinkType: nil,
                                            isFirstScheduledForDay: false, isCurrentLive: false)
    
    
    let activityFourAvoidLight = RosterActivity(activityType: .lightExposure,
                                                startDate: dateFormatter.date(from: "22/08/2018 08:00 +1000")!,
                                                endDate: nil,
                                                workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                                caffeineDrinkType: nil,
                                                isFirstScheduledForDay: false, isCurrentLive: false)
    
    let activityFiveSleep = RosterActivity(activityType: .sleep,
                                           startDate: dateFormatter.date(from: "22/08/2018 09:00 +1000")!,
                                           endDate: dateFormatter.date(from: "22/08/2018 15:00 +1000")!,
                                           workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                           caffeineDrinkType: nil,
                                           isFirstScheduledForDay: false, isCurrentLive: false)
    
    let activitySixDayOff = RosterActivity(activityType: .dayOff,
                                           startDate: dateFormatter.date(from: "23/08/2018 00:00 +1000")!,
                                           endDate: nil,
                                           workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                           caffeineDrinkType: nil,
                                           isFirstScheduledForDay: true, isCurrentLive: false)
    
    let activitySevenDayOff = RosterActivity(activityType: .dayOff,
                                             startDate: dateFormatter.date(from: "24/08/2018 00:00 +1000")!,
                                             endDate: nil,
                                             workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                             caffeineDrinkType: nil,
                                             isFirstScheduledForDay: true, isCurrentLive: false)
    
    let activityEightWork = RosterActivity(activityType: .work,
                                           startDate: dateFormatter.date(from: "25/08/2018 23:00 +1000")!,
                                           endDate: dateFormatter.date(from: "26/08/2018 07:00 +1000")!,
                                           workShiftType: "Night Shift", workNotes: "", workOvertimeHours: 0,
                                           caffeineDrinkType: nil,
                                           isFirstScheduledForDay: true, isCurrentLive: false)
    
    // Copy
    
    let activityNineWork = RosterActivity(activityType: .work,
                                         startDate: dateFormatter.date(from: "27/08/2018 10:00 +1000")!,
                                         endDate: dateFormatter.date(from: "27/08/2018 11:00 +1000")!,
                                         workShiftType: "Watch House",
                                         workNotes: "This is a note I added earlier on today, which is a couple of lines long.",
                                         workOvertimeHours: 2,
                                         caffeineDrinkType: nil,
                                         isFirstScheduledForDay: true, isCurrentLive: true)
    
    let activityTenCaffeine = RosterActivity(activityType: .caffeine,
                                             startDate: dateFormatter.date(from: "27/08/2018 12:00 +1000")!,
                                             endDate: dateFormatter.date(from: "28/08/2018 04:00 +1000")!,
                                             workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                             caffeineDrinkType: "coffee",
                                             isFirstScheduledForDay: false, isCurrentLive: true)
    
    let activityElevenOther = RosterActivity(activityType: .avoidLight,
                                            startDate: dateFormatter.date(from: "28/08/2018 07:15 +1000")!,
                                            endDate: dateFormatter.date(from: "28/08/2018 07:25 +1000")!,
                                            workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                            caffeineDrinkType: nil,
                                            isFirstScheduledForDay: false, isCurrentLive: false)
    
    
    let activityTwelveAvoidLight = RosterActivity(activityType: .lightExposure,
                                                startDate: dateFormatter.date(from: "28/08/2018 08:00 +1000")!,
                                                endDate: nil,
                                                workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                                caffeineDrinkType: nil,
                                                isFirstScheduledForDay: false, isCurrentLive: false)
    
    let activityThirteenSleep = RosterActivity(activityType: .sleep,
                                           startDate: dateFormatter.date(from: "28/08/2018 09:00 +1000")!,
                                           endDate: nil,
                                           workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                           caffeineDrinkType: nil,
                                           isFirstScheduledForDay: false, isCurrentLive: false)
    
    let activityFourteenDayOff = RosterActivity(activityType: .dayOff,
                                           startDate: dateFormatter.date(from: "29/08/2018 00:00 +1000")!,
                                           endDate: nil,
                                           workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                           caffeineDrinkType: nil,
                                           isFirstScheduledForDay: true, isCurrentLive: false)
    
    let activityFifteenDayOff = RosterActivity(activityType: .dayOff,
                                             startDate: dateFormatter.date(from: "30/08/2018 00:00 +1000")!,
                                             endDate: nil,
                                             workShiftType: nil, workNotes: nil, workOvertimeHours: nil,
                                             caffeineDrinkType: nil,
                                             isFirstScheduledForDay: true, isCurrentLive: false)
    
    let activitySixteenWork = RosterActivity(activityType: .work,
                                           startDate: dateFormatter.date(from: "31/08/2018 23:00 +1000")!,
                                           endDate: dateFormatter.date(from: "1/09/2018 07:00 +1000")!,
                                           workShiftType: "Night Shift", workNotes: "", workOvertimeHours: 0,
                                           caffeineDrinkType: nil,
                                           isFirstScheduledForDay: true, isCurrentLive: false)
    

    return [
      activityOneWork, activityTwoCaffeine, activityThreeOther, activityFourAvoidLight, activityFiveSleep,
      activitySixDayOff, activitySevenDayOff, activityEightWork,
      activityNineWork, activityTenCaffeine, activityElevenOther, activityTwelveAvoidLight, activityThirteenSleep,
      activityFourteenDayOff, activityFifteenDayOff, activitySixteenWork
    ]
  }
  
  static func addRosterShiftTimeExample() -> [SelectRosterShiftTime] {
    let timeOne = SelectRosterShiftTime(startTime: "07:00", endTime: "14:00", type: .timeRange)
    let timeTwo = SelectRosterShiftTime(startTime: "14:00", endTime: "23:00", type: .timeRange)
    let timeThree = SelectRosterShiftTime(startTime: "23:00", endTime: "07:00", type: .timeRange)
    let dayOff = SelectRosterShiftTime(startTime: "23:00", endTime: "07:00", type: .dayOff)
    let custom = SelectRosterShiftTime(startTime: "23:00", endTime: "07:00", type: .custom)

    return [timeOne, timeTwo, timeThree, dayOff, custom]
  }
  
  static func addRosterShiftTypeExample() -> [SelectRosterShiftType] {
    let typeOne = SelectRosterShiftType(typeName: "Assistant Watch House", isRemovable: true, isEditEnabled: false)
    let typeTwo = SelectRosterShiftType(typeName: "Watch House", isRemovable: true, isEditEnabled: false)
    let typeThree = SelectRosterShiftType(typeName: "Car", isRemovable: true, isEditEnabled: false)
    let typeFour = SelectRosterShiftType(typeName: "Van", isRemovable: true, isEditEnabled: false)
    
    return [typeOne, typeTwo, typeThree, typeFour, CreateShiftTypeCollectionDataStore.skipShiftType]
  }
}
