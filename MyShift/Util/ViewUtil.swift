//
//  ViewUtil.swift
//  MyShift
//
//  Created by Michael Tigas on 16/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit
import FSCalendar

/**
 This util class contains any View related accessors/extensions
 */
struct ViewUtil {
  private init() {
  }
  
  static var primaryColor                   = UIColor(red: 2/255.0, green: 36/255.0, blue: 99/255.0, alpha: 255.0)
  static var standardButtonBorderColor      = UIColor(red: 108/255.0, green: 198/255.0, blue: 255/255.0, alpha: 255/255.0)
  static var rosterActivityBackgroundColor  = UIColor(red: 10/255.0, green: 55/255.0, blue: 122/255.0, alpha: 255/255.0)
  static var overtimeBorderColor            = UIColor(red: 151/255.0, green: 151/255.0, blue: 151/255.0, alpha: 255/255.0)
  
  static var shiftTimeTypeBorderColor       = UIColor(red: 208/255.0, green: 208/255.0, blue: 208/255.0, alpha: 255/255.0)
  
  static var shiftTimeSelectBackgroundNormalColor   = UIColor(red: 28/255.0, green: 75/255.0, blue: 145/255.0, alpha: 255/255.0)
  static var shiftTimeSelectBackgroundSelectedColor = UIColor(red: 13/255.0, green: 74/255.0, blue: 147/255.0, alpha: 255/255.0)

  // Project defined Storyboard's
  private enum StoryboardFile {
    case addRoster, addShiftTimes, caffeine,
         commute, infoPopup, location,
         notifications, onboarding, roster,
         settings
    
    var title: String {
      switch self {
      case .addRoster:      return "AddRoster"
      case .addShiftTimes:  return "AddShiftTimes"
      case .caffeine:       return "Caffeine"
      case .commute:        return "Commute"
      case .infoPopup:      return "InfoPopup"
      case .location:       return "Location"
      case .notifications:  return "Notifications"
      case .onboarding:     return "Onboarding"
      case .roster:         return "Roster"
      case .settings:       return "Settings"
      }
    }
    
    func create() -> UIStoryboard {
      return UIStoryboard(name: self.title, bundle: nil)
    }
  }
  
  
  
  
  // MARK: - UINavigationController

  // MARK: Onboarding

  static var onboardingNavigationController: OnboardingNavigationController {
    get { return StoryboardFile.onboarding.create().instantiateViewController(withIdentifier: OnboardingNavigationController.storyboardId) as! OnboardingNavigationController }
  }
  
  // MARK: Roster
  
  static var rosterNavigationController: RosterNavigationController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: RosterNavigationController.storyboardId) as! RosterNavigationController }
  }
  
  static var addRosterNavigationController: AddRosterNavigationController {
    get { return StoryboardFile.addRoster.create().instantiateViewController(withIdentifier: AddRosterNavigationController.storyboardId) as! AddRosterNavigationController }
  }
  
  // MARK: Add Roster

  static var createSingleShiftNavigationController: CreateSingleShiftNavigationController {
    get { return StoryboardFile.addRoster.create().instantiateViewController(withIdentifier: CreateSingleShiftNavigationController.storyboardId) as! CreateSingleShiftNavigationController }
  }
  
  static func createSingleShiftNavigationController(successCallback: @escaping (() -> ()),
                                                 cancelCallback: @escaping (() -> ()),
                                                 contextDate: Date?) -> CreateSingleShiftNavigationController {
    let vc = ViewUtil.createSingleShiftNavigationController
    vc.successCallback = successCallback
    vc.cancelCallback = cancelCallback
    vc.contextDate = contextDate
    
    return vc
  }
  
  // MARK: Info Popup

  static var infoPopupNavigationController: InfoPopupNavigationController {
    get { return StoryboardFile.infoPopup.create().instantiateViewController(withIdentifier: InfoPopupNavigationController.storyboardId) as! InfoPopupNavigationController }
  }
  
  static func infoPopupNavigationController(withViewController: UIViewController) -> InfoPopupNavigationController {
    let vc = ViewUtil.infoPopupNavigationController
    vc.childVc = withViewController
    
    return vc
  }
  
  // MARK: Location
  
  static var locationSearchNavigationController: LocationSearchNavigationController {
    get { return StoryboardFile.location.create().instantiateViewController(withIdentifier: LocationSearchNavigationController.storyboardId) as! LocationSearchNavigationController }
  }
  
  static func locationSearchNavigationController(withCallback: @escaping (LocationNameCoordsResult) -> ()) -> LocationSearchNavigationController {
    let vc = ViewUtil.locationSearchNavigationController
    vc.completionCallback = withCallback

    return vc
  }
  
  // MARK: Settings
  
  static var settingsNavigationController: SettingsNavigationController {
    get { return StoryboardFile.settings.create().instantiateViewController(withIdentifier: SettingsNavigationController.storyboardId) as! SettingsNavigationController }
  }
  
  static var settingsCaffeinePreferenceNavigationController: SettingsCaffeinePreferenceNavigationController {
    get { return StoryboardFile.settings.create().instantiateViewController(withIdentifier: SettingsCaffeinePreferenceNavigationController.storyboardId) as! SettingsCaffeinePreferenceNavigationController }
  }
  
  static func settingsCaffeinePreferenceNavigationController(withCallback: @escaping (Bool) -> ()) -> SettingsCaffeinePreferenceNavigationController {
    let vc = ViewUtil.settingsCaffeinePreferenceNavigationController
    vc.completionCallback = withCallback
    
    return vc
  }
  
  static var settingsCommutePreferenceNavigationController: SettingsCommutePreferenceNavigationController {
    get { return StoryboardFile.settings.create().instantiateViewController(withIdentifier: SettingsCommutePreferenceNavigationController.storyboardId) as! SettingsCommutePreferenceNavigationController }
  }
  
  static func settingsCommutePreferenceNavigationController(withCallback: @escaping (Bool) -> ()) -> SettingsCommutePreferenceNavigationController {
    let vc = ViewUtil.settingsCommutePreferenceNavigationController
    vc.completionCallback = withCallback
    
    return vc
  }
  
  static var settingsShiftTimesPreferenceNavigationController: SettingsShiftTimesPreferenceNavigationController {
    get { return StoryboardFile.settings.create().instantiateViewController(withIdentifier: SettingsShiftTimesPreferenceNavigationController.storyboardId) as! SettingsShiftTimesPreferenceNavigationController }
  }
  
  static func settingsShiftTimesPreferenceNavigationController(withCallback: @escaping (Bool) -> ()) -> SettingsShiftTimesPreferenceNavigationController {
    let vc = ViewUtil.settingsShiftTimesPreferenceNavigationController
    vc.completionCallback = withCallback
    
    return vc
  }
  

  
  
  // MARK: - UIViewController

  // MARK: Onboarding
  
  static var onboardingStartViewController: OnboardingStartViewController {
    get { return StoryboardFile.onboarding.create().instantiateViewController(withIdentifier: OnboardingStartViewController.storyboardId) as! OnboardingStartViewController }
  }
  
  static var rosterInputInfoViewController: RosterInputInfoViewController {
    get { return StoryboardFile.onboarding.create().instantiateViewController(withIdentifier: RosterInputInfoViewController.storyboardId) as! RosterInputInfoViewController }
  }
  
  static var rosterInputSelectViewController: RosterInputSelectViewController {
    get { return StoryboardFile.onboarding.create().instantiateViewController(withIdentifier: RosterInputSelectViewController.storyboardId) as! RosterInputSelectViewController }
  }
  
  static var yourWorkRosterViewController: YourWorkRosterViewController {
    get { return StoryboardFile.onboarding.create().instantiateViewController(withIdentifier: YourWorkRosterViewController.storyboardId) as! YourWorkRosterViewController }
  }
  
  // MARK: Location

  static var stationAddressViewController: StationAddressViewController {
    get { return StoryboardFile.location.create().instantiateViewController(withIdentifier: StationAddressViewController.storyboardId) as! StationAddressViewController }
  }
  
  static var locationSearchViewController: LocationSearchViewController {
    get { return StoryboardFile.location.create().instantiateViewController(withIdentifier: LocationSearchViewController.storyboardId) as! LocationSearchViewController }
  }
  
  static func locationSearchViewController(withCallback: @escaping (LocationNameCoordsResult) -> ()) -> LocationSearchViewController {
    let vc = ViewUtil.locationSearchViewController
    vc.completionCallback = withCallback
    
    return vc
  }

  // MARK: Commute

  static var commuteTimeViewController: CommuteTimeViewController {
    get { return StoryboardFile.commute.create().instantiateViewController(withIdentifier: CommuteTimeViewController.storyboardId) as! CommuteTimeViewController }
  }

  // MARK: Add Shift Times
  
  static var addShiftTimesViewController: AddShiftTimesViewController {
    get { return StoryboardFile.addShiftTimes.create().instantiateViewController(withIdentifier: AddShiftTimesViewController.storyboardId) as! AddShiftTimesViewController }
  }
  
  // MARK: Caffeine
  
  static var caffeinePreferenceViewController: CaffeinePreferenceViewController {
    get { return StoryboardFile.caffeine.create().instantiateViewController(withIdentifier: CaffeinePreferenceViewController.storyboardId) as! CaffeinePreferenceViewController }
  }
  
  
  // MARK: Notifications
  
  static var notificationPreferencesViewController: NotificationPreferencesViewController {
    get { return StoryboardFile.notifications.create().instantiateViewController(withIdentifier: NotificationPreferencesViewController.storyboardId) as! NotificationPreferencesViewController }
  }
  
  static var notificationPreferencesInfoViewController: NotificationPreferencesInfoViewController {
    get { return StoryboardFile.notifications.create().instantiateViewController(withIdentifier: NotificationPreferencesInfoViewController.storyboardId) as! NotificationPreferencesInfoViewController }
  }
  
  // MARK: Roster

  static var rosterListViewController: RosterListViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: RosterListViewController.storyboardId) as! RosterListViewController }
  }
  
  static var scheduleListViewController: ScheduleListViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: ScheduleListViewController.storyboardId) as! ScheduleListViewController }
  }
  
  static var avoidLightDetailsViewController: AvoidLightDetailsViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: AvoidLightDetailsViewController.storyboardId) as! AvoidLightDetailsViewController }
  }
  
  static func avoidLightDetailsViewController(withActivity: RosterActivity) -> AvoidLightDetailsViewController {
    let vc = ViewUtil.avoidLightDetailsViewController
    vc.rosterActivity = withActivity

    return vc
  }
  
  static var lightExposureDetailsViewController: LightExposureDetailsViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: LightExposureDetailsViewController.storyboardId) as! LightExposureDetailsViewController }
  }
  
  static func lightExposureDetailsViewController(withActivity: RosterActivity) -> LightExposureDetailsViewController {
    let vc = ViewUtil.lightExposureDetailsViewController
    vc.rosterActivity = withActivity
    
    return vc
  }
  
  static var caffeineDetailsViewController: CaffeineDetailsViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: CaffeineDetailsViewController.storyboardId) as! CaffeineDetailsViewController }
  }
  
  static func caffeineDetailsViewController(withActivity: RosterActivity) -> CaffeineDetailsViewController {
    let vc = ViewUtil.caffeineDetailsViewController
    vc.rosterActivity = withActivity
    
    return vc
  }
  
  static var dayOffDetailsViewController: DayOffDetailsViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: DayOffDetailsViewController.storyboardId) as! DayOffDetailsViewController }
  }
  
  static func dayOffDetailsViewController(withActivity: RosterActivity) -> DayOffDetailsViewController {
    let vc = ViewUtil.dayOffDetailsViewController
    vc.rosterActivity = withActivity
    
    return vc
  }
  
  static var sleepDetailsViewController: SleepDetailsViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: SleepDetailsViewController.storyboardId) as! SleepDetailsViewController }
  }
  
  static func sleepDetailsViewController(withActivity: RosterActivity) -> SleepDetailsViewController {
    let vc = ViewUtil.sleepDetailsViewController
    vc.rosterActivity = withActivity
    
    return vc
  }

  static var workDetailsViewController: WorkDetailsViewController {
    get { return StoryboardFile.roster.create().instantiateViewController(withIdentifier: WorkDetailsViewController.storyboardId) as! WorkDetailsViewController }
  }
  
  static func workDetailsViewController(withActivity: RosterActivity) -> WorkDetailsViewController {
    let vc = ViewUtil.workDetailsViewController
    vc.rosterActivity = withActivity
    
    return vc
  }

  // MARK: Add Roster

  static var createShiftViewController: CreateShiftViewController {
    get { return StoryboardFile.addRoster.create().instantiateViewController(withIdentifier: CreateShiftViewController.storyboardId) as! CreateShiftViewController }
  }

  static func createShiftViewController(withDate: Date, pageControlPosition: Int?) -> CreateShiftViewController {
    let vc = ViewUtil.createShiftViewController
    vc.contextDate = withDate
    vc.pageControlPosition = pageControlPosition
    
    return vc
  }

  static var selectDatesViewController: SelectDatesViewController {
    get { return StoryboardFile.addRoster.create().instantiateViewController(withIdentifier: SelectDatesViewController.storyboardId) as! SelectDatesViewController }
  }

  static var addRosterCompleteViewController: AddRosterCompleteViewController {
    get { return StoryboardFile.addRoster.create().instantiateViewController(withIdentifier: AddRosterCompleteViewController.storyboardId) as! AddRosterCompleteViewController }
  }

  static func addRosterCompleteViewController(withShiftTotal: Int) -> AddRosterCompleteViewController {
    let vc = ViewUtil.addRosterCompleteViewController
    vc.totalShiftCreatedCount = withShiftTotal
    
    return vc
  }
  
  // MARK: Info Popup
  
  static var caffeineInfoViewController: CaffeineInfoViewController {
    get { return StoryboardFile.infoPopup.create().instantiateViewController(withIdentifier: CaffeineInfoViewController.storyboardId) as! CaffeineInfoViewController }
  }
  
  static var lightInfoViewController: LightInfoViewController {
    get { return StoryboardFile.infoPopup.create().instantiateViewController(withIdentifier: LightInfoViewController.storyboardId) as! LightInfoViewController }
  }
  
  
  static var sleepInfoViewController: SleepInfoViewController {
    get { return StoryboardFile.infoPopup.create().instantiateViewController(withIdentifier: SleepInfoViewController.storyboardId) as! SleepInfoViewController }
  }
  
  // MARK: Settings

  static var settingsViewController: SettingsViewController {
    get { return StoryboardFile.settings.create().instantiateViewController(withIdentifier: SettingsViewController.storyboardId) as! SettingsViewController }
  }

  
  
  
  
  // MARK: - Dialog Creation

  /**
   Create and return a basic popup dialog
   */
  static func createBasicPopupDialog(title: String,
                                     message: String,
                                     handler: ((UIAlertAction) -> Void)?,
                                     secondHandler: ((UIAlertAction) -> Void)? = nil,
                                     secondHandlerTitle: String? = nil) -> UIAlertController {
    let alertController = UIAlertController(
      title: title,
      message: message,
      preferredStyle: .alert)
    alertController.view.tintColor = ViewUtil.primaryColor
    
    let cancelAction: UIAlertAction
    if let handler = handler {
      cancelAction = UIAlertAction(
        title: "OK",
        style: .cancel,
        handler: handler)
    } else {
      cancelAction = UIAlertAction(
        title: "OK",
        style: .cancel,
        handler: nil)
    }
    
    alertController.addAction(cancelAction)
    
    // Add secondary button and handler - e.g. Cancel
    if let secondHandler = secondHandler,
      let title = secondHandlerTitle {
      let secondaryAction = UIAlertAction(
        title: title,
        style: .default,
        handler: secondHandler)
      
      alertController.addAction(secondaryAction)
    }
    
    return alertController
  }
  
  /**
   Open Settings app with current app screen
   */
  static func openSettingsApp() {
    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else { return }
    
    if UIApplication.shared.canOpenURL(settingsUrl) {
      if #available(iOS 10.0, *) {
        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
        })
      } else {
        UIApplication.shared.openURL(settingsUrl as URL)
      }
    }
  }
}

// MARK: - UIButton
extension UIButton {
  /**
   Apply a "Call To Action" button style
   */
  func ctaStyle() {
    self.layer.masksToBounds = true
    self.layer.cornerRadius = 25
    
    self.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
  }

  /**
   Apply a common button style
   */
  func standardButtonStyle() {
    self.layer.masksToBounds = true
    self.layer.cornerRadius = 18
    self.layer.borderWidth = 1
    self.layer.borderColor = ViewUtil.standardButtonBorderColor.cgColor
    
    self.tintColor = .white
    
    self.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
  }
}

// MARK: - UIView
extension UIView {
  
  /**
   Set rounded left corner borders
   */
  func roundedLeftBorder(radius: CGFloat) {
    if #available(iOS 11.0, *) {
      self.clipsToBounds = false
      self.layer.cornerRadius = radius
      self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    } else {
      let path = UIBezierPath(roundedRect: self.bounds,
                              byRoundingCorners: [.topLeft, .bottomLeft],
                              cornerRadii: CGSize(width: radius, height: radius))
      let mask = CAShapeLayer()
      mask.path = path.cgPath
      
      self.layer.mask = mask
    }
  }

  /**
   Set rounded right corner borders
   */
  func roundedRightBorder(radius: CGFloat) {
    if #available(iOS 11.0, *) {
      self.clipsToBounds = false
      self.layer.cornerRadius = radius
      self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    } else {
      let path = UIBezierPath(roundedRect: self.bounds,
                              byRoundingCorners: [.topRight, .bottomRight],
                              cornerRadii: CGSize(width: radius, height: radius))
      let mask = CAShapeLayer()
      mask.path = path.cgPath
      
      self.layer.mask = mask
    }
  }
}

// MARK: - UILabel
extension UILabel {
  /**
   Add a dotted underline attribute to existing label text
   */
  func underline() {
    guard let textString = self.text else {
      return
    }
    
    let attributedString = NSMutableAttributedString(string: textString)

    attributedString.addAttribute(NSAttributedStringKey.underlineStyle,
                                  value: NSUnderlineStyle.styleSingle.rawValue | NSUnderlineStyle.patternDot.rawValue,
                                  range: NSRange(location: 0, length: attributedString.length))
    
    self.attributedText = attributedString
  }
  
  /**
   Apply a UIFont style to various snippets contained within a complete string
   */
  func applyFontWeightStyle(_ style: UIFont.Weight, size: CGFloat, snippets: [String], completeString: String) {
    let attributedString = NSMutableAttributedString.init(string: completeString)

    for snippet in snippets {
      let stringRange = (completeString as NSString).range(of: snippet)

      attributedString.addAttribute(NSAttributedStringKey.font,
                                    value: UIFont.systemFont(ofSize: size, weight: style), range: stringRange)
    }

    self.attributedText = attributedString
  }
}

// MARK: - FSCalendar
extension FSCalendar {
  /**
   Apply a custom calendar styling to suit the theme of the app
   */
  func applyCustomStyle() {
    self.scrollEnabled = false // Disable calendar month scrolling
    self.scope = .month // Display Month view
    self.placeholderType = .none // Do not display a view for opposing Calendar month days

    self.headerHeight = 0 // Hide calendar header
    self.weekdayHeight = 0 // Hide top weekday row
    
    // Hide text selection color
    self.appearance.titleDefaultColor = .clear
    self.appearance.titleSelectionColor = .clear
    self.appearance.todayColor = .clear
    self.appearance.todaySelectionColor = .clear
    self.appearance.titleTodayColor = .clear
    self.appearance.subtitleTodayColor = .clear
    
    // Enable multiple date selection with finger swipe gesture
    self.allowsMultipleSelection = true
    self.swipeToChooseGesture.isEnabled = true
    self.swipeToChooseGesture.minimumPressDuration = 0.3
  }
}

// MARK: - UINavigationController
extension UINavigationController {
  /**
   Replace the standard iOS back button indicator with a custom image
   */
  func useCustomBackIndicator() {
    let imgBack = UIImage(named: "ic_back_nav")
    
    self.navigationBar.backIndicatorImage = imgBack
    self.navigationBar.backIndicatorTransitionMaskImage = imgBack
  }
}
