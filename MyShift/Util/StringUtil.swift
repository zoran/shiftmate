//
//  StringUtil.swift
//  MyShift
//
//  Created by Michael Tigas on 20/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import Foundation

/**
 This util class contains any String related accessors/extensions
 */
struct StringUtil {
  private init() {
  }
}

// MARK: - TimeInterval
extension TimeInterval {
  /**
   Format time as "23 hours, 59 minutes"
   */
  func format() -> String? {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.day, .hour, .minute, .second, .nanosecond]
    formatter.unitsStyle = .full
    formatter.maximumUnitCount = 2
    return formatter.string(from: self)
  }
}

// MARK: - Date
extension Date {
  /**
   Return date formatted as "August 2018"
   */
  func monthYear() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "LLLL YYYY"
    
    return formatter.string(from: self)
  }
  
  /**
   Return date formatted as "August"
   */
  func month() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "LLLL"
    
    return formatter.string(from: self)
  }
  
  /**
   Return date formatted as "21" or "1"
   */
  func dayDate() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "d"

    return formatter.string(from: self)
  }
  
  /**
   Return date formatted as "21st"
   */
  func dayDateSuffix() -> String {
    let dayDate = self.dayDate() // "21"
    
    let suffix: String
    switch dayDate {
    case "1", "21", "31": suffix = "st"
    case "2", "22":       suffix = "nd"
    case "3", "23":       suffix = "rd"
    default:              suffix = "th"
    }

    return "\(dayDate)\(suffix)"
  }

  /**
   Return date formatted as "Tue"
   */
  func dayName() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "E"
    
    return formatter.string(from: self)
  }
  
  /**
   Return date formatted as "Tuesday"
   */
  func dayNameFull() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "EEEE"
    
    return formatter.string(from: self)
  }
  
  /**
   Return date formatted as "23:00" or "03:00"
   */
  func timeTwentyFourHr() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    
    return formatter.string(from: self)
  }
}

// MARK: - [Date]
extension Sequence where Element == Date {
  /**
   Order dates from earliest to latest
   */
  func orderAscending() -> [Date] {
    var orderedList: [Date] = self as! [Date]

    
    orderedList.sort { firstDate, secondDate in
      return firstDate < secondDate
    }
    
    return orderedList
  }
}
