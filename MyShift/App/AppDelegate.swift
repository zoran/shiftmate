//
//  AppDelegate.swift
//  MyShift
//
//  Created by Michael Tigas on 15/8/18.
//  Copyright © 2018 Michael Tigas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    resolveInitialViewController()

    return true
  }
  
  /**
   Configure app UI state and display necessary initial navigation controller
   */
  private func resolveInitialViewController() {
    // Set clear background for UINavigationBar
    UINavigationBar.appearance().isTranslucent = true
    UINavigationBar.appearance().backgroundColor = .clear
    UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    UINavigationBar.appearance().shadowImage = UIImage()
    
    // Set white title font/buttons for UINavigationBar
    UINavigationBar.appearance().titleTextAttributes =
      [NSAttributedStringKey.foregroundColor : UIColor.white]
    UINavigationBar.appearance().tintColor = .white

    // Bar Button font
    UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.classForCoder() as! UIAppearanceContainer.Type])
      .setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], for: .normal)
    UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.classForCoder() as! UIAppearanceContainer.Type])
      .setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], for: .highlighted)

    // UISearchBar Input text
    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = .white
    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white] // Applies foreground color to Search Bar's set as Navigation Item title

    // ZORAN: This would be a great place to check whether the user has completed onboarding,
    // and open ViewUtil.rosterNavigationController instead
    let viewController = ViewUtil.onboardingNavigationController
    
    self.window = UIWindow(frame: UIScreen.main.bounds)
    self.window?.rootViewController = viewController
    self.window?.makeKeyAndVisible()
  }
}
